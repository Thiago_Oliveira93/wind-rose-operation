{
    "id": "ba66c02b-010c-4ed3-9dbd-374046a0d739",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_soldier_walk_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 18,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09152e0f-1826-4a88-988c-5e3f16e34491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba66c02b-010c-4ed3-9dbd-374046a0d739",
            "compositeImage": {
                "id": "b56c3378-ad61-494a-83f9-f51a4b872410",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09152e0f-1826-4a88-988c-5e3f16e34491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ccf8b58-9110-4191-94f2-74205bb29b0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09152e0f-1826-4a88-988c-5e3f16e34491",
                    "LayerId": "b2bc22a7-0e98-4f4c-a80d-3c4719251a47"
                }
            ]
        },
        {
            "id": "a8fd7360-9f07-4f60-affd-346bd83fef1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba66c02b-010c-4ed3-9dbd-374046a0d739",
            "compositeImage": {
                "id": "babf9fdb-fc56-4bb6-8830-819e9fa1364e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8fd7360-9f07-4f60-affd-346bd83fef1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ffcc4e7-f611-44ee-92bd-45143c72124d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8fd7360-9f07-4f60-affd-346bd83fef1c",
                    "LayerId": "b2bc22a7-0e98-4f4c-a80d-3c4719251a47"
                }
            ]
        },
        {
            "id": "aeaddc8a-658f-4271-b114-44fc94215016",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba66c02b-010c-4ed3-9dbd-374046a0d739",
            "compositeImage": {
                "id": "1c272ebf-7138-4957-9cf4-62cf9ae538d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aeaddc8a-658f-4271-b114-44fc94215016",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4c54bc9-9b99-46d6-9364-4626f0d2dc92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aeaddc8a-658f-4271-b114-44fc94215016",
                    "LayerId": "b2bc22a7-0e98-4f4c-a80d-3c4719251a47"
                }
            ]
        },
        {
            "id": "da9eebeb-1059-4dc2-9aca-ad4fa0fe97f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba66c02b-010c-4ed3-9dbd-374046a0d739",
            "compositeImage": {
                "id": "85a69251-61ce-489f-925c-f9e65cc630b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da9eebeb-1059-4dc2-9aca-ad4fa0fe97f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94c00bd7-26c0-45e9-823b-bc131a6c9a3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da9eebeb-1059-4dc2-9aca-ad4fa0fe97f4",
                    "LayerId": "b2bc22a7-0e98-4f4c-a80d-3c4719251a47"
                }
            ]
        },
        {
            "id": "2a2acf98-85b0-4c65-b6a2-271b14f15562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba66c02b-010c-4ed3-9dbd-374046a0d739",
            "compositeImage": {
                "id": "5b1c259f-9875-42fa-baae-98952cdd98cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a2acf98-85b0-4c65-b6a2-271b14f15562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97292d5b-e131-4d26-96a8-af3ba1789238",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a2acf98-85b0-4c65-b6a2-271b14f15562",
                    "LayerId": "b2bc22a7-0e98-4f4c-a80d-3c4719251a47"
                }
            ]
        },
        {
            "id": "2214fc49-7dbc-4ded-b86b-4601514f3da3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba66c02b-010c-4ed3-9dbd-374046a0d739",
            "compositeImage": {
                "id": "3b69ba9a-3a8e-460e-82ff-ff150dedbb76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2214fc49-7dbc-4ded-b86b-4601514f3da3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ecf4df3-c39a-4923-a5b8-5625f8cacab7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2214fc49-7dbc-4ded-b86b-4601514f3da3",
                    "LayerId": "b2bc22a7-0e98-4f4c-a80d-3c4719251a47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b2bc22a7-0e98-4f4c-a80d-3c4719251a47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba66c02b-010c-4ed3-9dbd-374046a0d739",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}
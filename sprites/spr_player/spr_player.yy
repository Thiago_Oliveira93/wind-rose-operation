{
    "id": "08a80468-2c77-4a3a-a75a-adfebb97fe97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d121b0e3-a6ef-4b28-b9b9-c64f56be36aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08a80468-2c77-4a3a-a75a-adfebb97fe97",
            "compositeImage": {
                "id": "c84df37b-c439-438b-9109-0a23811378f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d121b0e3-a6ef-4b28-b9b9-c64f56be36aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be8e80ac-4a27-4975-b46c-7cb236e8f6c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d121b0e3-a6ef-4b28-b9b9-c64f56be36aa",
                    "LayerId": "58ea7de3-8c9e-485e-96a2-d2ed3a7bc18d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "58ea7de3-8c9e-485e-96a2-d2ed3a7bc18d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08a80468-2c77-4a3a-a75a-adfebb97fe97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
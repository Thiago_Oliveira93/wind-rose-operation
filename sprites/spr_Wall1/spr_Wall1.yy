{
    "id": "2cebaae0-2031-4217-be8d-77f9eb514adb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Wall1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f745b2f9-a9ed-400e-9cb2-c11033b3e2e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cebaae0-2031-4217-be8d-77f9eb514adb",
            "compositeImage": {
                "id": "a173a3a4-1781-41cb-9be0-f93661b7baea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f745b2f9-a9ed-400e-9cb2-c11033b3e2e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49a3e583-c948-401e-938e-c232155687c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f745b2f9-a9ed-400e-9cb2-c11033b3e2e5",
                    "LayerId": "d9b7d64b-a010-48aa-ad1c-77c76d985e9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d9b7d64b-a010-48aa-ad1c-77c76d985e9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2cebaae0-2031-4217-be8d-77f9eb514adb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "882fd9dd-ca0f-488a-8ecb-a7bb7cc65338",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2560edd-d332-4886-84a8-5afef3d47d7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "882fd9dd-ca0f-488a-8ecb-a7bb7cc65338",
            "compositeImage": {
                "id": "dda08a30-7862-44e5-8a65-ee2a2bd8bd1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2560edd-d332-4886-84a8-5afef3d47d7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "704675a9-786e-4055-96b1-7524736c3109",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2560edd-d332-4886-84a8-5afef3d47d7d",
                    "LayerId": "c1430c4d-63a1-44ff-98b9-bf07dc61058c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c1430c4d-63a1-44ff-98b9-bf07dc61058c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "882fd9dd-ca0f-488a-8ecb-a7bb7cc65338",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
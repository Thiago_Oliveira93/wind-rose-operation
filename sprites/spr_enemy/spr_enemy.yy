{
    "id": "c929a54e-0184-4ddb-9124-d04962cacca0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c283da77-952d-400a-b184-24cacf191dee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c929a54e-0184-4ddb-9124-d04962cacca0",
            "compositeImage": {
                "id": "3377cb15-4a85-4aec-92c7-3ff96f69819e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c283da77-952d-400a-b184-24cacf191dee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb6f5646-9350-4a41-8e01-874203650cce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c283da77-952d-400a-b184-24cacf191dee",
                    "LayerId": "46f7f535-6b3e-41d0-94ed-d3c7ce5b81db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "46f7f535-6b3e-41d0-94ed-d3c7ce5b81db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c929a54e-0184-4ddb-9124-d04962cacca0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
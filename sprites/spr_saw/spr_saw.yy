{
    "id": "d7321c18-59ce-48c4-9ac4-56e5fc7cd0e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_saw",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0cbfaf2-0590-44c3-b7b7-f77686cee920",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7321c18-59ce-48c4-9ac4-56e5fc7cd0e6",
            "compositeImage": {
                "id": "0215469c-57ac-4139-9524-ce04514661dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0cbfaf2-0590-44c3-b7b7-f77686cee920",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c55eee2-5a1c-46f1-905e-5c15d4c0e332",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0cbfaf2-0590-44c3-b7b7-f77686cee920",
                    "LayerId": "52c4cf8c-ef6d-4203-9849-6f53b18a853a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "52c4cf8c-ef6d-4203-9849-6f53b18a853a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7321c18-59ce-48c4-9ac4-56e5fc7cd0e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
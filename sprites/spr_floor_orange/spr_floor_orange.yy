{
    "id": "ffac3c98-77cb-4ece-98ca-0da25e082571",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_floor_orange",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ac72004-a239-4bdd-abe9-fe5aeeb794ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffac3c98-77cb-4ece-98ca-0da25e082571",
            "compositeImage": {
                "id": "965f1f07-3552-4ed5-9216-b9a8803ad02d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ac72004-a239-4bdd-abe9-fe5aeeb794ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "727fcf57-9fcb-49b5-8317-292ceb01a6c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ac72004-a239-4bdd-abe9-fe5aeeb794ed",
                    "LayerId": "22f3e262-7243-4e80-9b69-ce4fdfc00b92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "22f3e262-7243-4e80-9b69-ce4fdfc00b92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ffac3c98-77cb-4ece-98ca-0da25e082571",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
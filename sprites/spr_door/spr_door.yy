{
    "id": "d60cc1b6-69b2-4c4e-9e73-5e7aa7eda25f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19bda4db-0974-471b-8ff4-d020caf8eefc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d60cc1b6-69b2-4c4e-9e73-5e7aa7eda25f",
            "compositeImage": {
                "id": "c620df0d-3d50-43a6-be2e-08c7e9c9976c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19bda4db-0974-471b-8ff4-d020caf8eefc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3899e17-780d-4576-8396-7061e29d70fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19bda4db-0974-471b-8ff4-d020caf8eefc",
                    "LayerId": "12b52f32-88c4-4567-adca-2bc0b1f8b1c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "12b52f32-88c4-4567-adca-2bc0b1f8b1c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d60cc1b6-69b2-4c4e-9e73-5e7aa7eda25f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "191e4142-829d-41fd-9a7f-a7500f620577",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_card",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a55cb48-56d3-4566-8180-7b1205abf1e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "191e4142-829d-41fd-9a7f-a7500f620577",
            "compositeImage": {
                "id": "65e4d725-66ba-447c-aefd-e188f1cb5d06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a55cb48-56d3-4566-8180-7b1205abf1e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f0efc6a-9365-49c9-9691-6079de74f00a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a55cb48-56d3-4566-8180-7b1205abf1e4",
                    "LayerId": "74e17223-c4e0-4edc-9153-08aba97e7fcc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "74e17223-c4e0-4edc-9153-08aba97e7fcc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "191e4142-829d-41fd-9a7f-a7500f620577",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 4
}
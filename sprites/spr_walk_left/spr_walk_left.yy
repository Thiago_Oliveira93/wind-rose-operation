{
    "id": "8e73d5e0-9a9a-408b-9b7b-af9774bb6beb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8465695a-2d2c-4dc7-a0d6-f8f2e089e7f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e73d5e0-9a9a-408b-9b7b-af9774bb6beb",
            "compositeImage": {
                "id": "dcf67ef7-f0f8-4a1f-bc56-40460b8d22ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8465695a-2d2c-4dc7-a0d6-f8f2e089e7f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7515887f-aad5-4e2f-914a-0a5c7343f711",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8465695a-2d2c-4dc7-a0d6-f8f2e089e7f1",
                    "LayerId": "a6b5123b-6164-4f5f-8ef5-130aacb9f7dc"
                }
            ]
        },
        {
            "id": "8cd3f6c4-91cc-4c69-9114-16bee9862ef3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e73d5e0-9a9a-408b-9b7b-af9774bb6beb",
            "compositeImage": {
                "id": "10eab64e-361f-41d4-8da6-19e1909a444f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cd3f6c4-91cc-4c69-9114-16bee9862ef3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f04aeaa0-f525-4b56-9fad-6399cbe6333c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cd3f6c4-91cc-4c69-9114-16bee9862ef3",
                    "LayerId": "a6b5123b-6164-4f5f-8ef5-130aacb9f7dc"
                }
            ]
        },
        {
            "id": "ff50d4c1-221b-4f96-960f-b41834654842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e73d5e0-9a9a-408b-9b7b-af9774bb6beb",
            "compositeImage": {
                "id": "6aec6fc0-8959-4ff6-b6d3-b699585c5dbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff50d4c1-221b-4f96-960f-b41834654842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08f8ff0d-3ddb-4ae4-a149-df158ff6bdfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff50d4c1-221b-4f96-960f-b41834654842",
                    "LayerId": "a6b5123b-6164-4f5f-8ef5-130aacb9f7dc"
                }
            ]
        },
        {
            "id": "b73ec07d-8978-459b-af32-d4ad96bd42c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e73d5e0-9a9a-408b-9b7b-af9774bb6beb",
            "compositeImage": {
                "id": "7181eadd-b401-4c94-b81b-6a0feae8ad34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b73ec07d-8978-459b-af32-d4ad96bd42c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce42ce5e-718a-474a-80c4-a42ad1e207f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b73ec07d-8978-459b-af32-d4ad96bd42c2",
                    "LayerId": "a6b5123b-6164-4f5f-8ef5-130aacb9f7dc"
                }
            ]
        },
        {
            "id": "96f30026-8956-45c8-b0a2-f9fd0855bfd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e73d5e0-9a9a-408b-9b7b-af9774bb6beb",
            "compositeImage": {
                "id": "10510d9c-1aa5-4174-8d12-531769b05429",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96f30026-8956-45c8-b0a2-f9fd0855bfd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dfbc2f2-5208-4ffe-b56e-9d037d72b7d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96f30026-8956-45c8-b0a2-f9fd0855bfd3",
                    "LayerId": "a6b5123b-6164-4f5f-8ef5-130aacb9f7dc"
                }
            ]
        },
        {
            "id": "4ad3d604-15a0-43d9-9114-51dd463fa3f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e73d5e0-9a9a-408b-9b7b-af9774bb6beb",
            "compositeImage": {
                "id": "ff2cceff-8676-4712-b32e-66c91cd383d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ad3d604-15a0-43d9-9114-51dd463fa3f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d78cc78d-f077-4b34-b398-ab18ca862fd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ad3d604-15a0-43d9-9114-51dd463fa3f7",
                    "LayerId": "a6b5123b-6164-4f5f-8ef5-130aacb9f7dc"
                }
            ]
        },
        {
            "id": "6b7db504-b5d4-489f-8b5e-9c1a47032f49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e73d5e0-9a9a-408b-9b7b-af9774bb6beb",
            "compositeImage": {
                "id": "4913b4bb-cd60-42f2-971b-27b09ebf8536",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b7db504-b5d4-489f-8b5e-9c1a47032f49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd647124-110b-4e79-8f92-7792c6456c82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b7db504-b5d4-489f-8b5e-9c1a47032f49",
                    "LayerId": "a6b5123b-6164-4f5f-8ef5-130aacb9f7dc"
                }
            ]
        },
        {
            "id": "6d9f173e-afe2-4da6-af95-d6bef505c13e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e73d5e0-9a9a-408b-9b7b-af9774bb6beb",
            "compositeImage": {
                "id": "ff96df03-91a7-4e78-9c3c-bd890e4e1b35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d9f173e-afe2-4da6-af95-d6bef505c13e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "270c7bd9-c039-4cbb-804a-8010c38616b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d9f173e-afe2-4da6-af95-d6bef505c13e",
                    "LayerId": "a6b5123b-6164-4f5f-8ef5-130aacb9f7dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a6b5123b-6164-4f5f-8ef5-130aacb9f7dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e73d5e0-9a9a-408b-9b7b-af9774bb6beb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 0,
    "yorig": 0
}
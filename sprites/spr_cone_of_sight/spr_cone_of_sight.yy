{
    "id": "e231ff35-c99e-49b3-ab27-68803eac3eab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cone_of_sight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "302489f7-d5f0-4144-9334-3192bae4c941",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e231ff35-c99e-49b3-ab27-68803eac3eab",
            "compositeImage": {
                "id": "a6811fb0-1e11-4e82-a9ea-e239ba2403d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "302489f7-d5f0-4144-9334-3192bae4c941",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f216409-6208-4174-9797-97c7e95b1159",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "302489f7-d5f0-4144-9334-3192bae4c941",
                    "LayerId": "38a38eb3-54e7-4f56-aad3-668d670f34b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "38a38eb3-54e7-4f56-aad3-668d670f34b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e231ff35-c99e-49b3-ab27-68803eac3eab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 6,
    "yorig": 33
}
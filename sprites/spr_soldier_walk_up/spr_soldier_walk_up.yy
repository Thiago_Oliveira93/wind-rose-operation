{
    "id": "0f0677da-1001-44e2-b2a9-d57a2832908e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_soldier_walk_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff159698-7783-4c6e-8c51-dfc42d7776bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f0677da-1001-44e2-b2a9-d57a2832908e",
            "compositeImage": {
                "id": "9d1cce23-f74f-4b92-8f7a-6d1f2698909a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff159698-7783-4c6e-8c51-dfc42d7776bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "555a6e16-ae84-4d05-837c-f656c53f24b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff159698-7783-4c6e-8c51-dfc42d7776bb",
                    "LayerId": "8310b932-77da-4dbc-98b3-0286bef45ff4"
                }
            ]
        },
        {
            "id": "85944d5a-8315-4e29-b846-a29e7999c24e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f0677da-1001-44e2-b2a9-d57a2832908e",
            "compositeImage": {
                "id": "72975f30-17aa-411a-b768-a8629122a097",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85944d5a-8315-4e29-b846-a29e7999c24e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02389106-07f9-4367-a956-43de6d04144c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85944d5a-8315-4e29-b846-a29e7999c24e",
                    "LayerId": "8310b932-77da-4dbc-98b3-0286bef45ff4"
                }
            ]
        },
        {
            "id": "38401d59-05ef-4bcb-ae17-8f2a55568b77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f0677da-1001-44e2-b2a9-d57a2832908e",
            "compositeImage": {
                "id": "209d9905-61cb-485a-9c9d-bc299493bdb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38401d59-05ef-4bcb-ae17-8f2a55568b77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92f3d81d-6047-41d7-9d24-d7750d02881f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38401d59-05ef-4bcb-ae17-8f2a55568b77",
                    "LayerId": "8310b932-77da-4dbc-98b3-0286bef45ff4"
                }
            ]
        },
        {
            "id": "e6e6a33c-0640-432a-9ff9-120faaae73c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f0677da-1001-44e2-b2a9-d57a2832908e",
            "compositeImage": {
                "id": "a2de2b9a-7fb2-48e9-bcd4-022c1fb2e303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6e6a33c-0640-432a-9ff9-120faaae73c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3279cfa-7ca4-473a-96e6-19fe77552d42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6e6a33c-0640-432a-9ff9-120faaae73c6",
                    "LayerId": "8310b932-77da-4dbc-98b3-0286bef45ff4"
                }
            ]
        },
        {
            "id": "1df1cbe0-2d89-4737-8e4e-6136618c03c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f0677da-1001-44e2-b2a9-d57a2832908e",
            "compositeImage": {
                "id": "02b8387a-2a90-4e6f-a301-e0938d2b7bf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1df1cbe0-2d89-4737-8e4e-6136618c03c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c1c098a-d35f-4b68-a352-d5ec32973616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1df1cbe0-2d89-4737-8e4e-6136618c03c2",
                    "LayerId": "8310b932-77da-4dbc-98b3-0286bef45ff4"
                }
            ]
        },
        {
            "id": "aaf98660-8417-4709-bf1c-411761105047",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f0677da-1001-44e2-b2a9-d57a2832908e",
            "compositeImage": {
                "id": "57c76355-a44f-4ec2-9bf4-f9ae5df0c701",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaf98660-8417-4709-bf1c-411761105047",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0197c5f5-0e90-48b0-b17b-062e51933310",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaf98660-8417-4709-bf1c-411761105047",
                    "LayerId": "8310b932-77da-4dbc-98b3-0286bef45ff4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8310b932-77da-4dbc-98b3-0286bef45ff4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f0677da-1001-44e2-b2a9-d57a2832908e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}
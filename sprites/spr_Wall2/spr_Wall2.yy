{
    "id": "44604518-35f0-4163-a232-01217fb913e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Wall2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf159970-73f5-43ee-8870-03e9255497be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44604518-35f0-4163-a232-01217fb913e6",
            "compositeImage": {
                "id": "80a32877-e047-4e26-ba17-6d973d082845",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf159970-73f5-43ee-8870-03e9255497be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60ad1eef-6f2c-40ec-ae59-f046dac2395e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf159970-73f5-43ee-8870-03e9255497be",
                    "LayerId": "5b59a334-b3a3-4664-8446-c87505874b7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5b59a334-b3a3-4664-8446-c87505874b7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44604518-35f0-4163-a232-01217fb913e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
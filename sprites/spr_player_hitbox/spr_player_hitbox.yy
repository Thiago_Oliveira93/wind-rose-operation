{
    "id": "c9d859fe-5198-4274-90e6-886c4387bf70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_hitbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50502303-b626-4770-8d9b-18705304e5d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9d859fe-5198-4274-90e6-886c4387bf70",
            "compositeImage": {
                "id": "18b23833-2538-42d3-8d13-4eb411f0631a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50502303-b626-4770-8d9b-18705304e5d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cd9e3b1-cf6a-4b3e-8087-1cdece56c0e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50502303-b626-4770-8d9b-18705304e5d4",
                    "LayerId": "b8a32992-ee61-4457-b80c-4f25646c9f01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b8a32992-ee61-4457-b80c-4f25646c9f01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9d859fe-5198-4274-90e6-886c4387bf70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
{
    "id": "1eeed485-37ce-4da0-94fc-6c15a1bbcd6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_teste_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 167,
    "bbox_left": 80,
    "bbox_right": 303,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4cb3aae8-1e9a-4dd1-837a-42d2f2742bd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1eeed485-37ce-4da0-94fc-6c15a1bbcd6b",
            "compositeImage": {
                "id": "15e3d21d-c023-4070-a2f6-2a561768c77e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cb3aae8-1e9a-4dd1-837a-42d2f2742bd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79ac8fa6-2bcb-4b81-9797-3a40daef844f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cb3aae8-1e9a-4dd1-837a-42d2f2742bd3",
                    "LayerId": "f5f70e9e-2618-4bfb-bab8-c0ca74b11652"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "f5f70e9e-2618-4bfb-bab8-c0ca74b11652",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1eeed485-37ce-4da0-94fc-6c15a1bbcd6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}
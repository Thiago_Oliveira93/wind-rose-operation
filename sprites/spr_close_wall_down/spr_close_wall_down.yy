{
    "id": "94c490f3-2c81-43f5-a630-3b73e1167194",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_close_wall_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 19,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5bff64f9-03bf-4a81-b5be-02ee113abee6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94c490f3-2c81-43f5-a630-3b73e1167194",
            "compositeImage": {
                "id": "49f5bc71-1764-4226-9c08-df4b4957e3e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bff64f9-03bf-4a81-b5be-02ee113abee6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b58b94eb-12cf-4516-b549-20a2a10235e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bff64f9-03bf-4a81-b5be-02ee113abee6",
                    "LayerId": "0ca794eb-6717-4460-9283-006492b7b09e"
                }
            ]
        },
        {
            "id": "fa56bcb7-90d7-4906-b58f-1433517ac31a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94c490f3-2c81-43f5-a630-3b73e1167194",
            "compositeImage": {
                "id": "060a694e-d001-4eb4-a8f3-8900e7c862ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa56bcb7-90d7-4906-b58f-1433517ac31a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c246c3f-20e2-4dd6-8ba3-21ac33070086",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa56bcb7-90d7-4906-b58f-1433517ac31a",
                    "LayerId": "0ca794eb-6717-4460-9283-006492b7b09e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0ca794eb-6717-4460-9283-006492b7b09e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94c490f3-2c81-43f5-a630-3b73e1167194",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}
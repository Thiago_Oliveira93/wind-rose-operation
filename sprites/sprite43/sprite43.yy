{
    "id": "5e2c2114-402a-4b42-a8ac-c531ee29542c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite43",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19106f72-1c2a-4024-aada-34d2b62ad3ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e2c2114-402a-4b42-a8ac-c531ee29542c",
            "compositeImage": {
                "id": "1da79685-bd52-4d7d-aab6-bac7133adc8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19106f72-1c2a-4024-aada-34d2b62ad3ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d032fc6-1449-4b82-81fd-402fc2879190",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19106f72-1c2a-4024-aada-34d2b62ad3ed",
                    "LayerId": "10915c65-e1e1-457d-bcc0-d0374322fc5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "10915c65-e1e1-457d-bcc0-d0374322fc5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e2c2114-402a-4b42-a8ac-c531ee29542c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
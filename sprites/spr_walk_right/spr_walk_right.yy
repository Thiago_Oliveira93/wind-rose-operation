{
    "id": "418aa879-bdc5-4ab9-a2b9-0f32d580d160",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f996e92b-02a6-4a38-8059-103bc7b85110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418aa879-bdc5-4ab9-a2b9-0f32d580d160",
            "compositeImage": {
                "id": "668c367f-dcf9-44e2-aa22-f50d7724b7c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f996e92b-02a6-4a38-8059-103bc7b85110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fd4b901-9cdb-45dc-8f9d-3b9b86e57d70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f996e92b-02a6-4a38-8059-103bc7b85110",
                    "LayerId": "f6707fa2-37de-4bd2-86f5-9b2c1252ee08"
                }
            ]
        },
        {
            "id": "ecd40861-5c8c-406f-90c7-c8b45b3fa8c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418aa879-bdc5-4ab9-a2b9-0f32d580d160",
            "compositeImage": {
                "id": "e0f20572-a94e-43ef-9248-88d14800d749",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecd40861-5c8c-406f-90c7-c8b45b3fa8c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51d29e53-2561-4be6-a5d9-02f87b46b407",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecd40861-5c8c-406f-90c7-c8b45b3fa8c6",
                    "LayerId": "f6707fa2-37de-4bd2-86f5-9b2c1252ee08"
                }
            ]
        },
        {
            "id": "5020e986-d13e-47ce-8e1a-9ba897239e9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418aa879-bdc5-4ab9-a2b9-0f32d580d160",
            "compositeImage": {
                "id": "9174eac7-e806-42f6-8a36-75032393d9f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5020e986-d13e-47ce-8e1a-9ba897239e9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e04f305-bddd-4193-bf34-8873f0bd9cab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5020e986-d13e-47ce-8e1a-9ba897239e9f",
                    "LayerId": "f6707fa2-37de-4bd2-86f5-9b2c1252ee08"
                }
            ]
        },
        {
            "id": "7be0cd93-0157-4dc6-95f5-df4fb389482e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418aa879-bdc5-4ab9-a2b9-0f32d580d160",
            "compositeImage": {
                "id": "38b2d0a4-3688-4d3e-b726-9f6a93c702bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7be0cd93-0157-4dc6-95f5-df4fb389482e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb0c502c-c3a0-4e8a-a0fa-57599baf1c76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7be0cd93-0157-4dc6-95f5-df4fb389482e",
                    "LayerId": "f6707fa2-37de-4bd2-86f5-9b2c1252ee08"
                }
            ]
        },
        {
            "id": "e058073f-fafb-4323-ba03-2502453fefa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418aa879-bdc5-4ab9-a2b9-0f32d580d160",
            "compositeImage": {
                "id": "a35e7080-115f-4491-b01c-5c99340ae4fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e058073f-fafb-4323-ba03-2502453fefa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bef51bf-cad6-4f00-950a-fd9847cca98a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e058073f-fafb-4323-ba03-2502453fefa7",
                    "LayerId": "f6707fa2-37de-4bd2-86f5-9b2c1252ee08"
                }
            ]
        },
        {
            "id": "d06683e6-4265-434e-b0e8-21a1877de506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418aa879-bdc5-4ab9-a2b9-0f32d580d160",
            "compositeImage": {
                "id": "217c0a51-7fa7-412c-88b8-5dd4cd49f5b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d06683e6-4265-434e-b0e8-21a1877de506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29154d7c-f953-4353-8f20-1bbf4ddf775d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d06683e6-4265-434e-b0e8-21a1877de506",
                    "LayerId": "f6707fa2-37de-4bd2-86f5-9b2c1252ee08"
                }
            ]
        },
        {
            "id": "940141e4-5438-458f-9260-fab7884dc761",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418aa879-bdc5-4ab9-a2b9-0f32d580d160",
            "compositeImage": {
                "id": "171b0524-e8f7-441a-b156-0131513f6815",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "940141e4-5438-458f-9260-fab7884dc761",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6cb10db-7e47-4f97-acbb-d4dcf541aac1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "940141e4-5438-458f-9260-fab7884dc761",
                    "LayerId": "f6707fa2-37de-4bd2-86f5-9b2c1252ee08"
                }
            ]
        },
        {
            "id": "dabc7316-362f-4317-9086-f8aac5f8cfb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418aa879-bdc5-4ab9-a2b9-0f32d580d160",
            "compositeImage": {
                "id": "9f792e3b-08ab-42a4-a6ce-60c976297dd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dabc7316-362f-4317-9086-f8aac5f8cfb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51dcf467-7561-4bb0-948d-9976a7546bf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dabc7316-362f-4317-9086-f8aac5f8cfb7",
                    "LayerId": "f6707fa2-37de-4bd2-86f5-9b2c1252ee08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f6707fa2-37de-4bd2-86f5-9b2c1252ee08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "418aa879-bdc5-4ab9-a2b9-0f32d580d160",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 0,
    "yorig": 0
}
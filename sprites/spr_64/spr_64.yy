{
    "id": "c651b125-0e51-4ce7-958b-ce86fd5b6a07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_64",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 20,
    "bbox_right": 40,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0cb8899-ad1c-4f0a-853c-2493764ff063",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c651b125-0e51-4ce7-958b-ce86fd5b6a07",
            "compositeImage": {
                "id": "aba19984-628c-45a0-a909-7313956796f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0cb8899-ad1c-4f0a-853c-2493764ff063",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b633b1fa-e7eb-4e26-930a-0fe0fe4f1850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0cb8899-ad1c-4f0a-853c-2493764ff063",
                    "LayerId": "e5d2dc3b-e14d-4a9d-a4e1-073d54120d3b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e5d2dc3b-e14d-4a9d-a4e1-073d54120d3b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c651b125-0e51-4ce7-958b-ce86fd5b6a07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 50,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
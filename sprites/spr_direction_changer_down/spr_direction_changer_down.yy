{
    "id": "f991fef3-00c5-47ab-86e4-6404c581b671",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_direction_changer_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2deead80-dfcd-4970-bded-53341ef7a335",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f991fef3-00c5-47ab-86e4-6404c581b671",
            "compositeImage": {
                "id": "305bb0a7-19fc-4d88-932c-8e17c3957a3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2deead80-dfcd-4970-bded-53341ef7a335",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e5c855f-30c0-4ba0-affd-99f192e74cd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2deead80-dfcd-4970-bded-53341ef7a335",
                    "LayerId": "35d0efa4-337d-47a9-b86a-ec54ef234ccd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "35d0efa4-337d-47a9-b86a-ec54ef234ccd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f991fef3-00c5-47ab-86e4-6404c581b671",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
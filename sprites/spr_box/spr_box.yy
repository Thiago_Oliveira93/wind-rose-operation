{
    "id": "88331037-c045-41b8-9c45-f7fb4d8d3f6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "295cacc7-c862-4057-a5c0-83f5168e97a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88331037-c045-41b8-9c45-f7fb4d8d3f6f",
            "compositeImage": {
                "id": "a13e7b53-4840-496d-b6df-439066d6b034",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "295cacc7-c862-4057-a5c0-83f5168e97a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cfd19a2-33b7-46d8-910f-00c29e456be2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "295cacc7-c862-4057-a5c0-83f5168e97a7",
                    "LayerId": "93b83995-6293-4059-8b69-ca359abdbaa4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "93b83995-6293-4059-8b69-ca359abdbaa4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88331037-c045-41b8-9c45-f7fb4d8d3f6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
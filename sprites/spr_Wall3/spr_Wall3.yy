{
    "id": "a0ecb625-9270-447e-aee0-511297690de6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Wall3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b39b7529-6277-4319-8835-97a2aca1f400",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0ecb625-9270-447e-aee0-511297690de6",
            "compositeImage": {
                "id": "e678e8f2-a9fb-4522-a839-616a39c5beb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b39b7529-6277-4319-8835-97a2aca1f400",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad3ef432-fb32-47b4-864e-a5d4d1b15e11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b39b7529-6277-4319-8835-97a2aca1f400",
                    "LayerId": "dd73deeb-541f-4e0b-81f8-1c412d9c4837"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "dd73deeb-541f-4e0b-81f8-1c412d9c4837",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0ecb625-9270-447e-aee0-511297690de6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
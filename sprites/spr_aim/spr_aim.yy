{
    "id": "2d81e3d6-ca0c-4995-b2b3-509c312e3926",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_aim",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 297,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a090d91-1bf8-4f2c-b6a4-1806c66d76b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d81e3d6-ca0c-4995-b2b3-509c312e3926",
            "compositeImage": {
                "id": "e4dfb56d-9103-4760-a280-0fadf0be4b6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a090d91-1bf8-4f2c-b6a4-1806c66d76b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "834b2560-6d91-42c3-866c-bcfe1fa3dd06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a090d91-1bf8-4f2c-b6a4-1806c66d76b0",
                    "LayerId": "8b19ee6b-a1aa-4443-89b0-459a6bb41dd5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "8b19ee6b-a1aa-4443-89b0-459a6bb41dd5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d81e3d6-ca0c-4995-b2b3-509c312e3926",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 150,
    "yorig": 150
}
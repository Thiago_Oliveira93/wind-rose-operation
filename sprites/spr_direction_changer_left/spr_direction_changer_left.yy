{
    "id": "94b501f2-360c-445d-9e9c-c510f4a31de7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_direction_changer_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "425c984d-4b55-435d-82dd-766d39e64a11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94b501f2-360c-445d-9e9c-c510f4a31de7",
            "compositeImage": {
                "id": "70cdb3a2-946e-43d5-a45e-0d1c213757d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "425c984d-4b55-435d-82dd-766d39e64a11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "563d3d99-a80e-42d3-9c3b-e5fb242eb0f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "425c984d-4b55-435d-82dd-766d39e64a11",
                    "LayerId": "a300d958-c414-445f-a330-5ea205ccf1f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a300d958-c414-445f-a330-5ea205ccf1f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94b501f2-360c-445d-9e9c-c510f4a31de7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
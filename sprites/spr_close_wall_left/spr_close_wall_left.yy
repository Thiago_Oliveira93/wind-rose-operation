{
    "id": "8fb1d433-b79a-4343-86d2-7e25596e9d67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_close_wall_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 4,
    "bbox_right": 17,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9d304c3-6101-4618-85a7-fdc53db96e2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb1d433-b79a-4343-86d2-7e25596e9d67",
            "compositeImage": {
                "id": "be72730b-fcb4-4faa-9b1c-b858887ebebe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9d304c3-6101-4618-85a7-fdc53db96e2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdede8a0-129d-4bba-a3fb-fc53c0ca6500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9d304c3-6101-4618-85a7-fdc53db96e2f",
                    "LayerId": "6ffec64e-48ce-4c19-aeb7-2b8246298b88"
                }
            ]
        },
        {
            "id": "03ab534a-f4e5-4a63-a060-e6634966dbd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb1d433-b79a-4343-86d2-7e25596e9d67",
            "compositeImage": {
                "id": "db840417-0bb7-4002-9efa-3235a3b2189a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03ab534a-f4e5-4a63-a060-e6634966dbd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c4300b0-b25e-49b5-9e92-6bfc6deae0c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03ab534a-f4e5-4a63-a060-e6634966dbd7",
                    "LayerId": "6ffec64e-48ce-4c19-aeb7-2b8246298b88"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6ffec64e-48ce-4c19-aeb7-2b8246298b88",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fb1d433-b79a-4343-86d2-7e25596e9d67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 8,
    "yorig": 0
}
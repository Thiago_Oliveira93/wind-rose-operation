{
    "id": "63bed941-43e8-4871-a34c-bb35b7a1fe83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullets",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1803e24b-ae81-46ab-a8ea-d8c543e2f43a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63bed941-43e8-4871-a34c-bb35b7a1fe83",
            "compositeImage": {
                "id": "d14900b3-4911-4efc-9a48-867862dfd6ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1803e24b-ae81-46ab-a8ea-d8c543e2f43a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae20b33c-633f-474b-8030-077d1bad7fcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1803e24b-ae81-46ab-a8ea-d8c543e2f43a",
                    "LayerId": "570aa8f2-f242-4b94-ac74-9002b3a236bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "570aa8f2-f242-4b94-ac74-9002b3a236bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63bed941-43e8-4871-a34c-bb35b7a1fe83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}
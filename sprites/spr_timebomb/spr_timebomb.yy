{
    "id": "5a28d5ca-f3f7-4c9d-b15b-dd37fcc6672c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_timebomb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b83401ad-da9f-41c1-b344-d6dde1ee2ca6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a28d5ca-f3f7-4c9d-b15b-dd37fcc6672c",
            "compositeImage": {
                "id": "8f2dfb91-0810-4e8e-80a9-76766f2b66b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b83401ad-da9f-41c1-b344-d6dde1ee2ca6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da760e28-c68c-403e-b418-93625df497d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b83401ad-da9f-41c1-b344-d6dde1ee2ca6",
                    "LayerId": "c26bc6f0-6e0c-46fb-bcc5-a9d7e9513657"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "c26bc6f0-6e0c-46fb-bcc5-a9d7e9513657",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a28d5ca-f3f7-4c9d-b15b-dd37fcc6672c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}
{
    "id": "d774852f-5f09-4dfd-b69b-f78f9c4410d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_machinegun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3fe62567-44a3-4392-9f8f-e58ca7161589",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d774852f-5f09-4dfd-b69b-f78f9c4410d3",
            "compositeImage": {
                "id": "cfa3bc3f-b133-4cc2-8d0b-251ddc0e00b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fe62567-44a3-4392-9f8f-e58ca7161589",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86ba6b61-f382-4b47-8e6c-ff551f85f90a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fe62567-44a3-4392-9f8f-e58ca7161589",
                    "LayerId": "4de8ff87-00f5-4c50-b373-673d1d2f00e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4de8ff87-00f5-4c50-b373-673d1d2f00e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d774852f-5f09-4dfd-b69b-f78f9c4410d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
{
    "id": "dd698a25-b1f9-4667-a97f-95b650b82eb4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_camera",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a8c40ff-bb7f-4e5c-af07-6cf3eee1530e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd698a25-b1f9-4667-a97f-95b650b82eb4",
            "compositeImage": {
                "id": "fde52ea8-9b64-4994-8d7d-158ee3f9012b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a8c40ff-bb7f-4e5c-af07-6cf3eee1530e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af4e7360-f846-4eb8-8f53-19dda9bb16f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a8c40ff-bb7f-4e5c-af07-6cf3eee1530e",
                    "LayerId": "56d01b5d-215d-495c-b874-6a050327a047"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "56d01b5d-215d-495c-b874-6a050327a047",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd698a25-b1f9-4667-a97f-95b650b82eb4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}
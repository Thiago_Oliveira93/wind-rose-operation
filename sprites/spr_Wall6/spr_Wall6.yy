{
    "id": "d883cf28-5683-4db0-9ab8-8a496fbe78e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Wall6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5935dc71-f956-4026-8e17-ebc0bea1d5b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d883cf28-5683-4db0-9ab8-8a496fbe78e5",
            "compositeImage": {
                "id": "e3d503c3-220b-42f6-901b-35329fc03c0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5935dc71-f956-4026-8e17-ebc0bea1d5b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe83b09e-28ff-471b-b5fb-0b40b2800482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5935dc71-f956-4026-8e17-ebc0bea1d5b9",
                    "LayerId": "2ddfd293-11a8-4a3e-b8df-9e3dc98b4930"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2ddfd293-11a8-4a3e-b8df-9e3dc98b4930",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d883cf28-5683-4db0-9ab8-8a496fbe78e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
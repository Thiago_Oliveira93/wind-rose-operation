{
    "id": "cc476249-bb99-4017-8260-78a178020b23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4cc0c48-8518-4f04-ab8f-d35b0bb6acf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc476249-bb99-4017-8260-78a178020b23",
            "compositeImage": {
                "id": "a0c450d8-d589-42fe-a34d-9b8569992ff5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4cc0c48-8518-4f04-ab8f-d35b0bb6acf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24d1bb91-da7a-4db0-81fa-3f1b7c0c163b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4cc0c48-8518-4f04-ab8f-d35b0bb6acf7",
                    "LayerId": "4dcfc25a-ebe0-4833-a256-dcbfc5e0a7e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4dcfc25a-ebe0-4833-a256-dcbfc5e0a7e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc476249-bb99-4017-8260-78a178020b23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
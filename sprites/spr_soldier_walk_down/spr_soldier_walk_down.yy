{
    "id": "3b6645d1-6af9-4616-8e39-eb910d61246f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_soldier_walk_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 4,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73ae809e-f799-4d9e-9dc4-2795d9d1ee8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b6645d1-6af9-4616-8e39-eb910d61246f",
            "compositeImage": {
                "id": "778af6f3-4165-4307-bfbe-99b804b0cd25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73ae809e-f799-4d9e-9dc4-2795d9d1ee8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f283880a-af02-41e0-9054-cf45b1c5b811",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73ae809e-f799-4d9e-9dc4-2795d9d1ee8c",
                    "LayerId": "794e9e00-5fdc-4047-88b1-246f24617a49"
                }
            ]
        },
        {
            "id": "4a08c230-07a3-48b7-8f9f-6c6e1f42ebfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b6645d1-6af9-4616-8e39-eb910d61246f",
            "compositeImage": {
                "id": "0cdca253-c660-4c8d-896c-1d8f9022c926",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a08c230-07a3-48b7-8f9f-6c6e1f42ebfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad391001-8de2-4fce-9192-b4f3063fb220",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a08c230-07a3-48b7-8f9f-6c6e1f42ebfc",
                    "LayerId": "794e9e00-5fdc-4047-88b1-246f24617a49"
                }
            ]
        },
        {
            "id": "64d7495f-de45-4d34-9146-d9ed87c08d76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b6645d1-6af9-4616-8e39-eb910d61246f",
            "compositeImage": {
                "id": "5ec142f3-2de4-4611-83ae-e34e78667519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64d7495f-de45-4d34-9146-d9ed87c08d76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71f376f7-ddac-433f-8434-ecaf5ce8fb6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64d7495f-de45-4d34-9146-d9ed87c08d76",
                    "LayerId": "794e9e00-5fdc-4047-88b1-246f24617a49"
                }
            ]
        },
        {
            "id": "82a12447-5c96-4144-9162-ddd93e6b8cfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b6645d1-6af9-4616-8e39-eb910d61246f",
            "compositeImage": {
                "id": "f588d3b1-cf18-4e48-951c-8aca671f52b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82a12447-5c96-4144-9162-ddd93e6b8cfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7a7ed2f-4024-4097-93e5-84a176e5e863",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82a12447-5c96-4144-9162-ddd93e6b8cfe",
                    "LayerId": "794e9e00-5fdc-4047-88b1-246f24617a49"
                }
            ]
        },
        {
            "id": "12bbf0a0-d44e-462c-8932-21fb2c4a9fdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b6645d1-6af9-4616-8e39-eb910d61246f",
            "compositeImage": {
                "id": "079ee98c-5c35-458d-82ac-62ffe9d487ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12bbf0a0-d44e-462c-8932-21fb2c4a9fdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d6c32a2-3b95-4d27-b538-4796da1fbb50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12bbf0a0-d44e-462c-8932-21fb2c4a9fdd",
                    "LayerId": "794e9e00-5fdc-4047-88b1-246f24617a49"
                }
            ]
        },
        {
            "id": "9dba0749-591f-456d-8db7-3492a0c04293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b6645d1-6af9-4616-8e39-eb910d61246f",
            "compositeImage": {
                "id": "bf887dde-0b1a-4470-aa4f-f6256a4b7c7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dba0749-591f-456d-8db7-3492a0c04293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "144be571-a963-453e-ac40-7035145ab4c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dba0749-591f-456d-8db7-3492a0c04293",
                    "LayerId": "794e9e00-5fdc-4047-88b1-246f24617a49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "794e9e00-5fdc-4047-88b1-246f24617a49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b6645d1-6af9-4616-8e39-eb910d61246f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}
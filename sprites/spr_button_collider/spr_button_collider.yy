{
    "id": "501231e6-bc15-4385-b055-f1da787e1e09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_collider",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8beb37f-f86e-4621-90e9-3403046c4a07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "501231e6-bc15-4385-b055-f1da787e1e09",
            "compositeImage": {
                "id": "71e6b67d-8f5a-4414-8758-4761fbdac42e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8beb37f-f86e-4621-90e9-3403046c4a07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07101092-7e5a-4507-9618-c7321a7343d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8beb37f-f86e-4621-90e9-3403046c4a07",
                    "LayerId": "f81afacc-218d-4b90-a635-83d60b3677fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f81afacc-218d-4b90-a635-83d60b3677fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "501231e6-bc15-4385-b055-f1da787e1e09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "ba399a06-de8d-4811-afee-c9f6374a898f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_room_transition_pwr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b2a3e81-d5b4-45ac-9533-90ae1aabdc39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba399a06-de8d-4811-afee-c9f6374a898f",
            "compositeImage": {
                "id": "0eb51ebe-77f4-4049-baa7-7791b54355cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b2a3e81-d5b4-45ac-9533-90ae1aabdc39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "866f2858-ad86-494f-b75a-83ca5bfd0639",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b2a3e81-d5b4-45ac-9533-90ae1aabdc39",
                    "LayerId": "95c40ecb-1b6c-4eaa-92cc-e40e63484be2"
                }
            ]
        },
        {
            "id": "75b6f4cb-79a7-40cb-bbb0-fb11936db014",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba399a06-de8d-4811-afee-c9f6374a898f",
            "compositeImage": {
                "id": "22642158-d979-4fa2-ac18-eea54033ed7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75b6f4cb-79a7-40cb-bbb0-fb11936db014",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58879413-2363-4e54-89df-6bca277d851e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75b6f4cb-79a7-40cb-bbb0-fb11936db014",
                    "LayerId": "95c40ecb-1b6c-4eaa-92cc-e40e63484be2"
                }
            ]
        },
        {
            "id": "668cddf4-3d4d-47dd-8fe0-9d20bcf7ac8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba399a06-de8d-4811-afee-c9f6374a898f",
            "compositeImage": {
                "id": "5ff8557f-46a2-4841-b9af-4dc470eeb50f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "668cddf4-3d4d-47dd-8fe0-9d20bcf7ac8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "760a574e-a9ae-4599-b222-cd2c73806493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "668cddf4-3d4d-47dd-8fe0-9d20bcf7ac8d",
                    "LayerId": "95c40ecb-1b6c-4eaa-92cc-e40e63484be2"
                }
            ]
        },
        {
            "id": "886642df-4cae-419c-a356-5fd9ba47ecd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba399a06-de8d-4811-afee-c9f6374a898f",
            "compositeImage": {
                "id": "54dbbf2b-9372-4a2b-a8ef-79714bb79ed9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "886642df-4cae-419c-a356-5fd9ba47ecd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6e0018b-d0ec-4c51-9063-6c93ffa4b324",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "886642df-4cae-419c-a356-5fd9ba47ecd1",
                    "LayerId": "95c40ecb-1b6c-4eaa-92cc-e40e63484be2"
                }
            ]
        },
        {
            "id": "13b093a9-0742-4bf4-a536-e84d53bfdfdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba399a06-de8d-4811-afee-c9f6374a898f",
            "compositeImage": {
                "id": "ff8ab14a-2d33-4901-91ce-2037937911a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13b093a9-0742-4bf4-a536-e84d53bfdfdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc072923-2e32-430c-b3d2-9fb6a3a3c164",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13b093a9-0742-4bf4-a536-e84d53bfdfdb",
                    "LayerId": "95c40ecb-1b6c-4eaa-92cc-e40e63484be2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "95c40ecb-1b6c-4eaa-92cc-e40e63484be2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba399a06-de8d-4811-afee-c9f6374a898f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": -8,
    "yorig": 0
}
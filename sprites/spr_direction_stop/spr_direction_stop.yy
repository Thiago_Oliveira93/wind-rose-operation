{
    "id": "def6d93e-0c93-4cb5-a0f6-597cb8446f6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_direction_stop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6018625c-644b-4877-885c-5a4fda4fb7d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def6d93e-0c93-4cb5-a0f6-597cb8446f6d",
            "compositeImage": {
                "id": "217e4782-edd0-471e-b68f-e958fb6f0cf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6018625c-644b-4877-885c-5a4fda4fb7d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43df5af6-56b8-4a7a-92a2-f85a2ec3d60d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6018625c-644b-4877-885c-5a4fda4fb7d9",
                    "LayerId": "560225ea-caf1-45b8-a77c-b6c86a50d37b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "560225ea-caf1-45b8-a77c-b6c86a50d37b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "def6d93e-0c93-4cb5-a0f6-597cb8446f6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
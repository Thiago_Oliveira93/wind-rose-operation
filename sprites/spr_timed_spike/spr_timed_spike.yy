{
    "id": "b36916fd-0958-4ed8-bf22-ededdfe0cbdd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_timed_spike",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56f97090-5f7c-4df3-8f1f-ca5f52923733",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b36916fd-0958-4ed8-bf22-ededdfe0cbdd",
            "compositeImage": {
                "id": "cab4b310-8f45-4903-b316-3f03fe5e8ae7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56f97090-5f7c-4df3-8f1f-ca5f52923733",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7288b26-9150-499b-ace6-6d7b4a02e1aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56f97090-5f7c-4df3-8f1f-ca5f52923733",
                    "LayerId": "d5c47c65-139a-4179-83da-54a2ddb8d643"
                }
            ]
        },
        {
            "id": "5b8e2fd0-497b-4601-b535-2998928b739b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b36916fd-0958-4ed8-bf22-ededdfe0cbdd",
            "compositeImage": {
                "id": "6e5bc2a1-d83d-4a3a-831f-72420b4b396d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b8e2fd0-497b-4601-b535-2998928b739b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72d70115-d84e-4804-8aa9-4956e0a26909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b8e2fd0-497b-4601-b535-2998928b739b",
                    "LayerId": "d5c47c65-139a-4179-83da-54a2ddb8d643"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d5c47c65-139a-4179-83da-54a2ddb8d643",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b36916fd-0958-4ed8-bf22-ededdfe0cbdd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
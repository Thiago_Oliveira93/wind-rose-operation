{
    "id": "17c18f15-e49b-45c6-b16d-0ca159fa37c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 1,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f109f611-b0d2-43f7-898f-9292d6de4176",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17c18f15-e49b-45c6-b16d-0ca159fa37c7",
            "compositeImage": {
                "id": "a889b361-06ae-490c-bec0-31c51567f733",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f109f611-b0d2-43f7-898f-9292d6de4176",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc617729-aa21-4048-8791-01d00cecf383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f109f611-b0d2-43f7-898f-9292d6de4176",
                    "LayerId": "7dbe2bd2-6b25-421b-a0c8-6cb0261265c5"
                }
            ]
        },
        {
            "id": "1e0abdab-b749-40c9-8054-7f531a767d68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17c18f15-e49b-45c6-b16d-0ca159fa37c7",
            "compositeImage": {
                "id": "0353ad57-8660-4f39-85ce-b3fdbf89ab56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e0abdab-b749-40c9-8054-7f531a767d68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c5d9529-7961-40a0-abd0-0a2fedf8345b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e0abdab-b749-40c9-8054-7f531a767d68",
                    "LayerId": "7dbe2bd2-6b25-421b-a0c8-6cb0261265c5"
                }
            ]
        },
        {
            "id": "a939e431-263e-4c54-90b4-6a49b829ef39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17c18f15-e49b-45c6-b16d-0ca159fa37c7",
            "compositeImage": {
                "id": "65660847-6135-4bab-b429-989095049f79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a939e431-263e-4c54-90b4-6a49b829ef39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6432dea8-1057-4a80-9966-73752a4b4020",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a939e431-263e-4c54-90b4-6a49b829ef39",
                    "LayerId": "7dbe2bd2-6b25-421b-a0c8-6cb0261265c5"
                }
            ]
        },
        {
            "id": "2234adf8-e465-4820-a0fd-cf9c1d557afc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17c18f15-e49b-45c6-b16d-0ca159fa37c7",
            "compositeImage": {
                "id": "fb1e731f-8b1a-4874-801d-f1e5dc0f82f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2234adf8-e465-4820-a0fd-cf9c1d557afc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d415d47-0ce6-41ba-a554-10def4e39f51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2234adf8-e465-4820-a0fd-cf9c1d557afc",
                    "LayerId": "7dbe2bd2-6b25-421b-a0c8-6cb0261265c5"
                }
            ]
        },
        {
            "id": "0b682ce9-4bd6-4d04-b2fb-35e2ccab66d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17c18f15-e49b-45c6-b16d-0ca159fa37c7",
            "compositeImage": {
                "id": "f751d10e-f496-469f-aff3-987dd49a3904",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b682ce9-4bd6-4d04-b2fb-35e2ccab66d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c6cd0d2-109f-4ceb-9cd9-b7ed0c250e15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b682ce9-4bd6-4d04-b2fb-35e2ccab66d8",
                    "LayerId": "7dbe2bd2-6b25-421b-a0c8-6cb0261265c5"
                }
            ]
        },
        {
            "id": "f8333128-9157-45bc-8d67-e90eba25aa1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17c18f15-e49b-45c6-b16d-0ca159fa37c7",
            "compositeImage": {
                "id": "c85706ca-6422-42cc-921f-8a182d5e7275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8333128-9157-45bc-8d67-e90eba25aa1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c0a85af-1480-4b0a-b717-7c161bac5728",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8333128-9157-45bc-8d67-e90eba25aa1a",
                    "LayerId": "7dbe2bd2-6b25-421b-a0c8-6cb0261265c5"
                }
            ]
        },
        {
            "id": "63b5e936-0a5a-4093-9629-4f50d3fbbc02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17c18f15-e49b-45c6-b16d-0ca159fa37c7",
            "compositeImage": {
                "id": "2f5816ac-629c-4eda-82a6-8d65c1d10311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63b5e936-0a5a-4093-9629-4f50d3fbbc02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0c4e23a-b50d-4b03-a44c-2ebdd98a8cf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63b5e936-0a5a-4093-9629-4f50d3fbbc02",
                    "LayerId": "7dbe2bd2-6b25-421b-a0c8-6cb0261265c5"
                }
            ]
        },
        {
            "id": "50663ffd-bc72-4eb1-8b1b-c3b8119e296b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17c18f15-e49b-45c6-b16d-0ca159fa37c7",
            "compositeImage": {
                "id": "f55d6a90-28b2-4095-8faf-ca9cffddd197",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50663ffd-bc72-4eb1-8b1b-c3b8119e296b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e96057c8-21f1-4639-b18e-4b8779ce01cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50663ffd-bc72-4eb1-8b1b-c3b8119e296b",
                    "LayerId": "7dbe2bd2-6b25-421b-a0c8-6cb0261265c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7dbe2bd2-6b25-421b-a0c8-6cb0261265c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17c18f15-e49b-45c6-b16d-0ca159fa37c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "15ed5b10-dfb2-4d64-85f9-e3265bbaece5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "168daa6b-92dd-4456-b75b-3d98ddcddb64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15ed5b10-dfb2-4d64-85f9-e3265bbaece5",
            "compositeImage": {
                "id": "3ab424af-7f5c-4771-9900-809018ae9c14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "168daa6b-92dd-4456-b75b-3d98ddcddb64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0265d12-1526-4f44-ba3b-e57e33c6f105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "168daa6b-92dd-4456-b75b-3d98ddcddb64",
                    "LayerId": "8ac32009-8071-4b94-a5fd-353895b04fb6"
                }
            ]
        },
        {
            "id": "3979de96-36a3-4498-9a38-a344bb6534c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15ed5b10-dfb2-4d64-85f9-e3265bbaece5",
            "compositeImage": {
                "id": "b0b06849-c9dc-4228-9d2c-5b2d22aa2be3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3979de96-36a3-4498-9a38-a344bb6534c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b4f5898-95d4-4869-83fc-58938a6883c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3979de96-36a3-4498-9a38-a344bb6534c0",
                    "LayerId": "8ac32009-8071-4b94-a5fd-353895b04fb6"
                }
            ]
        },
        {
            "id": "329b1039-a349-4e1a-8142-6075fa1de54a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15ed5b10-dfb2-4d64-85f9-e3265bbaece5",
            "compositeImage": {
                "id": "0298369b-be42-4fe6-b1da-4ae5bb2b4493",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "329b1039-a349-4e1a-8142-6075fa1de54a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbe03144-96d5-40df-82ba-8eb0a50622ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "329b1039-a349-4e1a-8142-6075fa1de54a",
                    "LayerId": "8ac32009-8071-4b94-a5fd-353895b04fb6"
                }
            ]
        },
        {
            "id": "9a436fab-0e27-47df-b557-3ccbc90b84c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15ed5b10-dfb2-4d64-85f9-e3265bbaece5",
            "compositeImage": {
                "id": "d854f233-e22e-4c6d-b2c4-59e0dd033d45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a436fab-0e27-47df-b557-3ccbc90b84c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60adc38b-61ee-46fc-98b2-f412e137c9f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a436fab-0e27-47df-b557-3ccbc90b84c4",
                    "LayerId": "8ac32009-8071-4b94-a5fd-353895b04fb6"
                }
            ]
        },
        {
            "id": "214db079-232c-4290-873e-c9d2f85f73f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15ed5b10-dfb2-4d64-85f9-e3265bbaece5",
            "compositeImage": {
                "id": "a89411a2-5555-46f7-8318-96ef6a48bb6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "214db079-232c-4290-873e-c9d2f85f73f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cb5dd8f-1d5a-4c8d-b428-7ec1e18b7f88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "214db079-232c-4290-873e-c9d2f85f73f8",
                    "LayerId": "8ac32009-8071-4b94-a5fd-353895b04fb6"
                }
            ]
        },
        {
            "id": "e5b265c7-348b-42ed-8924-4216d66b7067",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15ed5b10-dfb2-4d64-85f9-e3265bbaece5",
            "compositeImage": {
                "id": "b5b0e8d7-0ae3-4b97-be46-b85461f663f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5b265c7-348b-42ed-8924-4216d66b7067",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a973d20-413d-4661-aa28-2df67255afa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5b265c7-348b-42ed-8924-4216d66b7067",
                    "LayerId": "8ac32009-8071-4b94-a5fd-353895b04fb6"
                }
            ]
        },
        {
            "id": "b8d58167-54d4-4a49-a384-6882f6413a0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15ed5b10-dfb2-4d64-85f9-e3265bbaece5",
            "compositeImage": {
                "id": "a776f6fa-3927-4863-96f1-445c064b01d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8d58167-54d4-4a49-a384-6882f6413a0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2445ae2b-751d-4dc2-9ab2-c2354089da88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8d58167-54d4-4a49-a384-6882f6413a0f",
                    "LayerId": "8ac32009-8071-4b94-a5fd-353895b04fb6"
                }
            ]
        },
        {
            "id": "99f4fc01-dda0-4e36-8889-5b1bbc978b8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15ed5b10-dfb2-4d64-85f9-e3265bbaece5",
            "compositeImage": {
                "id": "8adaaaf8-eea2-4815-b869-9f7c6a30893e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99f4fc01-dda0-4e36-8889-5b1bbc978b8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e33a38e-bf54-4de7-8bc4-6588bd40e2d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99f4fc01-dda0-4e36-8889-5b1bbc978b8d",
                    "LayerId": "8ac32009-8071-4b94-a5fd-353895b04fb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8ac32009-8071-4b94-a5fd-353895b04fb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15ed5b10-dfb2-4d64-85f9-e3265bbaece5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "eb36f2f6-c1ef-4476-99b6-787c9852a905",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_direction_changer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0f3c6bd-2090-4429-b8aa-98316536ac44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb36f2f6-c1ef-4476-99b6-787c9852a905",
            "compositeImage": {
                "id": "7987f784-1370-41ef-a685-fc6dbbfda719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0f3c6bd-2090-4429-b8aa-98316536ac44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b280713b-83a2-4e55-81a5-e51d6572f50c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0f3c6bd-2090-4429-b8aa-98316536ac44",
                    "LayerId": "6f8e557d-4ca5-4ac6-8463-3ff6fe6670e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6f8e557d-4ca5-4ac6-8463-3ff6fe6670e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb36f2f6-c1ef-4476-99b6-787c9852a905",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "8f1ee375-f3e7-4ba5-a4a3-176c79921811",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_room_transition",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51fdce0d-ec8b-47cf-958c-5de231a806fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f1ee375-f3e7-4ba5-a4a3-176c79921811",
            "compositeImage": {
                "id": "1bdd904c-c096-439c-bc77-add7c3fe4c56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51fdce0d-ec8b-47cf-958c-5de231a806fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "508671af-79ee-4dad-af48-e1e8a8fd1102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51fdce0d-ec8b-47cf-958c-5de231a806fb",
                    "LayerId": "4e9e46f9-4ec1-4504-b0cd-ad31045e49c0"
                }
            ]
        },
        {
            "id": "d1478e9f-3ce3-447c-86fa-5340830a2415",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f1ee375-f3e7-4ba5-a4a3-176c79921811",
            "compositeImage": {
                "id": "02db4fbf-6c0e-4970-aaae-50cb164c8906",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1478e9f-3ce3-447c-86fa-5340830a2415",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f68e7930-3872-4180-9750-d539067a1156",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1478e9f-3ce3-447c-86fa-5340830a2415",
                    "LayerId": "4e9e46f9-4ec1-4504-b0cd-ad31045e49c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4e9e46f9-4ec1-4504-b0cd-ad31045e49c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f1ee375-f3e7-4ba5-a4a3-176c79921811",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
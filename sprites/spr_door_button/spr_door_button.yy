{
    "id": "cbadbad1-98f2-471b-b0c4-4beff93c08bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_door_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bb6697a-9597-4a06-ad29-e17c6e9d31c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbadbad1-98f2-471b-b0c4-4beff93c08bc",
            "compositeImage": {
                "id": "2caaf4d7-96e4-46bd-8174-0a7d70a2f7e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bb6697a-9597-4a06-ad29-e17c6e9d31c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05ea950b-1b51-4509-b66b-3d2e7d423d5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bb6697a-9597-4a06-ad29-e17c6e9d31c0",
                    "LayerId": "c9dbf78d-14e0-40b2-b1f9-fa1d189953a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c9dbf78d-14e0-40b2-b1f9-fa1d189953a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbadbad1-98f2-471b-b0c4-4beff93c08bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "2da05ea2-49d7-4d03-a5a2-b23760743f36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 19,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2109e2cc-a149-43b1-a5de-dc955ebb051a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2da05ea2-49d7-4d03-a5a2-b23760743f36",
            "compositeImage": {
                "id": "f7bfa5c1-0c15-4e28-8e91-12991decd647",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2109e2cc-a149-43b1-a5de-dc955ebb051a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08101ae9-4de6-4522-90e8-075bdb79162a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2109e2cc-a149-43b1-a5de-dc955ebb051a",
                    "LayerId": "b5b726a8-1eb3-4c30-9dc9-bfd14f20014c"
                }
            ]
        },
        {
            "id": "8430c957-071b-494f-8b68-177b49acde6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2da05ea2-49d7-4d03-a5a2-b23760743f36",
            "compositeImage": {
                "id": "77dbcd63-5d2a-4a79-9587-9c59a30e90af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8430c957-071b-494f-8b68-177b49acde6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12b5b073-a2f9-442e-a246-81f3b67eff8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8430c957-071b-494f-8b68-177b49acde6b",
                    "LayerId": "b5b726a8-1eb3-4c30-9dc9-bfd14f20014c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b5b726a8-1eb3-4c30-9dc9-bfd14f20014c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2da05ea2-49d7-4d03-a5a2-b23760743f36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "b4a33e19-d7d0-405c-ab96-b696dc2af9aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_direction_changer_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b882256-19ab-4c0c-85c9-5ef2516d1960",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4a33e19-d7d0-405c-ab96-b696dc2af9aa",
            "compositeImage": {
                "id": "f256f586-8cf1-452d-aa5c-e7a0eb3691d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b882256-19ab-4c0c-85c9-5ef2516d1960",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "803726a6-de13-48aa-800a-3e78dc07c17d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b882256-19ab-4c0c-85c9-5ef2516d1960",
                    "LayerId": "45eb1241-6779-403a-9586-cfd0594b0f37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "45eb1241-6779-403a-9586-cfd0594b0f37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4a33e19-d7d0-405c-ab96-b696dc2af9aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
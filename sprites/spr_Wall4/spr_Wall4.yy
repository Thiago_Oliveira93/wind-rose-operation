{
    "id": "63745172-50f2-4a60-91ff-2c12186a786f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Wall4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38b0a1db-0187-41b0-a6b0-c77115b5e518",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63745172-50f2-4a60-91ff-2c12186a786f",
            "compositeImage": {
                "id": "8f08f2cf-163d-4038-92c0-1c04c9b7263f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38b0a1db-0187-41b0-a6b0-c77115b5e518",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "979419ed-1461-4148-a7df-4813efee9153",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38b0a1db-0187-41b0-a6b0-c77115b5e518",
                    "LayerId": "9d43fa3e-2822-4ff0-8c8f-6c4d8d328f75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9d43fa3e-2822-4ff0-8c8f-6c4d8d328f75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63745172-50f2-4a60-91ff-2c12186a786f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
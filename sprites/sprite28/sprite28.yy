{
    "id": "169d863d-d62e-4a31-b58f-4c7d0387986d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite28",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "348cf856-ab31-4da1-9503-0b87edc9e3f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "169d863d-d62e-4a31-b58f-4c7d0387986d",
            "compositeImage": {
                "id": "a88fd43e-038e-4d29-908b-2464ca8dab08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "348cf856-ab31-4da1-9503-0b87edc9e3f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59499079-f7eb-48c9-86ce-e0eaad9c330b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "348cf856-ab31-4da1-9503-0b87edc9e3f5",
                    "LayerId": "607fd0af-43dd-40ba-b691-842989d08443"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "607fd0af-43dd-40ba-b691-842989d08443",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "169d863d-d62e-4a31-b58f-4c7d0387986d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 16
}
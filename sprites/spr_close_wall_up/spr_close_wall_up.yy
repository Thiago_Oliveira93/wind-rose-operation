{
    "id": "8e3fd85d-cf23-414b-8456-ca1c35bc0903",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_close_wall_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 19,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c70ee02d-aaa0-4aa1-8d99-360e5ee191d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e3fd85d-cf23-414b-8456-ca1c35bc0903",
            "compositeImage": {
                "id": "ea5bc679-7c18-4236-b66d-8b9b93a600e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c70ee02d-aaa0-4aa1-8d99-360e5ee191d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eeef11e-f187-4ab5-b2b7-2f0e286a5166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c70ee02d-aaa0-4aa1-8d99-360e5ee191d5",
                    "LayerId": "b68254d0-0f69-422e-824e-4a88acb9f7c0"
                }
            ]
        },
        {
            "id": "6a6c2234-5f8f-4294-99dc-ca705e52d41c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e3fd85d-cf23-414b-8456-ca1c35bc0903",
            "compositeImage": {
                "id": "288076e3-a066-4027-8f71-048eeab4da56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a6c2234-5f8f-4294-99dc-ca705e52d41c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35bf4793-a892-4afc-8a66-6e078f93dc43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a6c2234-5f8f-4294-99dc-ca705e52d41c",
                    "LayerId": "b68254d0-0f69-422e-824e-4a88acb9f7c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b68254d0-0f69-422e-824e-4a88acb9f7c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e3fd85d-cf23-414b-8456-ca1c35bc0903",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "61cff652-cf8c-4be3-a84e-192926aa5f1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_soldier_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 18,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48185e81-8abf-4b69-880a-ee59ef216c03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61cff652-cf8c-4be3-a84e-192926aa5f1d",
            "compositeImage": {
                "id": "d8efd14d-4a92-4a42-9b04-8e94022ebd53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48185e81-8abf-4b69-880a-ee59ef216c03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97b861a6-ae3a-49f3-9948-95571fa01799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48185e81-8abf-4b69-880a-ee59ef216c03",
                    "LayerId": "0f760989-caba-4daf-aa1a-bb671b6fdbec"
                }
            ]
        },
        {
            "id": "d52781cb-24f5-4812-9f5f-12c751737e54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61cff652-cf8c-4be3-a84e-192926aa5f1d",
            "compositeImage": {
                "id": "0adbdc73-f55b-4f0c-96c3-7be4ac6fceb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d52781cb-24f5-4812-9f5f-12c751737e54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44903dcc-c9cf-4c21-b3fc-79f92fd22ee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d52781cb-24f5-4812-9f5f-12c751737e54",
                    "LayerId": "0f760989-caba-4daf-aa1a-bb671b6fdbec"
                }
            ]
        },
        {
            "id": "fde81f88-3672-4a56-8245-dc17721d9f4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61cff652-cf8c-4be3-a84e-192926aa5f1d",
            "compositeImage": {
                "id": "fd871c83-d571-400d-8f30-54f5136df693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fde81f88-3672-4a56-8245-dc17721d9f4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55815dff-3de2-40f0-8b2f-3d30ffc99d5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fde81f88-3672-4a56-8245-dc17721d9f4c",
                    "LayerId": "0f760989-caba-4daf-aa1a-bb671b6fdbec"
                }
            ]
        },
        {
            "id": "91978d42-92b9-4248-93d2-1e1164dced32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61cff652-cf8c-4be3-a84e-192926aa5f1d",
            "compositeImage": {
                "id": "c479b499-3552-47ae-aebb-445c3d6f41ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91978d42-92b9-4248-93d2-1e1164dced32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b753989-2e15-4fc0-98d7-8e0848ae28ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91978d42-92b9-4248-93d2-1e1164dced32",
                    "LayerId": "0f760989-caba-4daf-aa1a-bb671b6fdbec"
                }
            ]
        },
        {
            "id": "44e1e17d-e103-4a57-a09b-c413e3dbbf18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61cff652-cf8c-4be3-a84e-192926aa5f1d",
            "compositeImage": {
                "id": "1547dea3-2e7f-4bb0-94d7-9b76a0cbf59e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44e1e17d-e103-4a57-a09b-c413e3dbbf18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c25ce5d7-c613-469e-b6ee-a0b1dce6e6d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44e1e17d-e103-4a57-a09b-c413e3dbbf18",
                    "LayerId": "0f760989-caba-4daf-aa1a-bb671b6fdbec"
                }
            ]
        },
        {
            "id": "f2dd5646-182c-482a-905e-4a83ccb3559a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61cff652-cf8c-4be3-a84e-192926aa5f1d",
            "compositeImage": {
                "id": "1fd379fd-6195-4b8b-b90c-9e06d7bfe005",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2dd5646-182c-482a-905e-4a83ccb3559a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e88296cd-dfbf-4620-8d24-adea5e37b36f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2dd5646-182c-482a-905e-4a83ccb3559a",
                    "LayerId": "0f760989-caba-4daf-aa1a-bb671b6fdbec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0f760989-caba-4daf-aa1a-bb671b6fdbec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61cff652-cf8c-4be3-a84e-192926aa5f1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}
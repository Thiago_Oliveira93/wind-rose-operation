{
    "id": "3a2c0805-fbf6-45e6-9836-9630e0cc4aaa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Wall5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b5cf627-7d12-4f6a-85cf-2dd9db1f947d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a2c0805-fbf6-45e6-9836-9630e0cc4aaa",
            "compositeImage": {
                "id": "239f47b7-1e61-4b90-8850-2d49a53b8351",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b5cf627-7d12-4f6a-85cf-2dd9db1f947d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d3460e0-0c0c-4129-9504-3ee9828f2b91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b5cf627-7d12-4f6a-85cf-2dd9db1f947d",
                    "LayerId": "7a07099e-d255-4104-9a6f-789394f804ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7a07099e-d255-4104-9a6f-789394f804ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a2c0805-fbf6-45e6-9836-9630e0cc4aaa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
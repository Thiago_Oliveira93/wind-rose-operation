{
    "id": "cd862c5a-85a1-482d-8184-6a1e30f1b4a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_close_wall_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 7,
    "bbox_right": 20,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff7835ad-4bed-4db5-a801-e89fc7c79fee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd862c5a-85a1-482d-8184-6a1e30f1b4a8",
            "compositeImage": {
                "id": "becd524c-68fc-49b5-9c75-0ba9e4d5616b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff7835ad-4bed-4db5-a801-e89fc7c79fee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bc2fff2-72c0-464c-98b1-4a3bea7a08d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff7835ad-4bed-4db5-a801-e89fc7c79fee",
                    "LayerId": "6fa73eb7-38b4-48e9-a1e8-60e5c3e579fc"
                }
            ]
        },
        {
            "id": "7948a629-920a-4667-aa0f-016d726928f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd862c5a-85a1-482d-8184-6a1e30f1b4a8",
            "compositeImage": {
                "id": "b5b2e43e-bb51-43a0-8676-f958a3f3662d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7948a629-920a-4667-aa0f-016d726928f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b018e59-86de-482c-ad22-0dd9ec6906db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7948a629-920a-4667-aa0f-016d726928f3",
                    "LayerId": "6fa73eb7-38b4-48e9-a1e8-60e5c3e579fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6fa73eb7-38b4-48e9-a1e8-60e5c3e579fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd862c5a-85a1-482d-8184-6a1e30f1b4a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": -12,
    "yorig": 0
}
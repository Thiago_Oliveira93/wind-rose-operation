{
    "id": "cf9639e6-d49c-480f-996d-0f06ebfd0aea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_floor_purple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7919f38-4c6b-4472-abcf-06f54bf8d06f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf9639e6-d49c-480f-996d-0f06ebfd0aea",
            "compositeImage": {
                "id": "c6e9d4b8-c322-47eb-8f2c-6b31ef53c5c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7919f38-4c6b-4472-abcf-06f54bf8d06f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b129a1c0-f4d1-47d0-bd00-629613cc9836",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7919f38-4c6b-4472-abcf-06f54bf8d06f",
                    "LayerId": "5f2335d9-3530-428d-b60d-4e1bac53c34f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5f2335d9-3530-428d-b60d-4e1bac53c34f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf9639e6-d49c-480f-996d-0f06ebfd0aea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
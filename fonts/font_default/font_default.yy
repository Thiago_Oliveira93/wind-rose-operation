{
    "id": "13e06f47-1acc-4466-b002-39c722bd947d",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_default",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Thintel",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "467dcd07-505f-42ae-8cc2-91f97760c345",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 52,
                "y": 68
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7b5b7b8c-a00c-47cf-8c5d-67ede0d63bfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 74,
                "y": 68
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "aea7bba8-bdc8-48aa-a67b-6829c284b620",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 169,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a7c7b09d-6e98-4330-a9ed-53dce43e849d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 20,
                "offset": -2,
                "shift": 11,
                "w": 11,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "757264b7-7dd4-4a6e-b10b-31d1a68bc001",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "aba2dfc8-92bd-4bcb-8044-a0d6ab9e41c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 20,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ac4914f9-ba23-49ea-9cda-4d52fe4f7f49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "1f606eab-a4ba-4f7c-8a6d-f599a5042f98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 66,
                "y": 68
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ebaea413-8316-4166-9c71-a1447289ba99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 10,
                "y": 68
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5716b6f1-1793-4563-90df-f83495e8ab58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 16,
                "y": 68
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "53d2cd3b-1d71-4d9e-8bc1-d9a59bf78f10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 153,
                "y": 46
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "26206be9-d819-4052-8026-dbb593dd89e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "aec623ba-fdb6-4367-860d-8d7fa762c70e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 28,
                "y": 68
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8f630125-eccd-4be8-ada6-8dc3672e83de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 46
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "03b296e7-be11-4dd1-932a-f4e1a9d2a1f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 82,
                "y": 68
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "7b06efa0-135e-4268-a531-c6dbaa5ccbbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 145,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "fe2f5e86-604e-4837-9a74-5389b0bd8b9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "90079e9d-8919-429b-9fe5-1b67479fbeb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 161,
                "y": 46
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "43f36f85-f95e-4787-a928-8b751ff1d1b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 245,
                "y": 24
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9749b661-c8e4-44dc-9221-5880a4e9555e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 218,
                "y": 24
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f824ab81-1701-45f3-8306-b2edbee02ac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 128,
                "y": 46
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "6a2144a1-08c0-425f-87bf-9f2ae52d701e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 200,
                "y": 24
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "da410648-74a5-46e8-965a-3390b769cebc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 209,
                "y": 24
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "36a44953-61b1-4ba5-bd30-5f4c93b9ed41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "137669ef-aa66-40ec-a0f2-7de9083593a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 227,
                "y": 24
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b6d2faf9-eb7d-432d-8173-0b06065be24c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 236,
                "y": 24
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "96bacb43-5deb-4e49-9191-e61b2936c295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 58,
                "y": 68
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "5b8a72ca-297b-4f39-9a26-72336db496cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 34,
                "y": 68
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "230b0de2-c139-4b76-99a9-2f379031b99b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 46
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "15bba230-4305-49f9-bce7-001f3268d941",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 47,
                "y": 46
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2330118c-fda6-4ff6-9394-a09341969629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 46
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8cec4b68-51c9-41d5-855e-7cac15458643",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 119,
                "y": 46
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e589020d-ac93-4681-88e6-2a8d4e7ef684",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b887073b-3a9e-4163-a068-b36abab29225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 56,
                "y": 46
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c345aea6-a86e-42f9-b4d8-f754314175cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 65,
                "y": 46
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "26234834-6053-4671-bc61-6c2533f37c42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 74,
                "y": 46
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "6282a505-1ef5-41e0-ae02-1e31dd7e2bce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 83,
                "y": 46
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "5009cb05-d532-4b44-b7b6-fc328f9dffc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 217,
                "y": 46
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a24f6bb9-abc5-4bf2-a99a-bd67dff122c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 209,
                "y": 46
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "29a20739-ce00-4d3b-8a79-65b265dafb48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 92,
                "y": 46
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a3e889ae-21d4-44ce-8268-ba8056c53cd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 101,
                "y": 46
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e7161c40-03d1-4cf5-8d89-0185db4121bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 201,
                "y": 46
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "8a1d7e1b-93ee-4450-bde4-2c1891dbfd71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 110,
                "y": 46
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0c0ea757-db21-4d4c-b570-d90ea2b29acf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 46
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c7130811-7c00-45fd-b79f-a39e072565b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 185,
                "y": 46
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ffe531c6-bfc5-4d63-a3f1-20d879efa8ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "786f2ccc-16a4-4b26-b1e7-f17d6972cd0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "dc5600d0-28f0-4579-8105-891de0f8adb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 182,
                "y": 24
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "26f5adee-ff23-4f8d-b5e7-70fdb0569189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "301fd5f6-0590-4c29-877e-bc1dec916d9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 24
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "eb40b915-a9b3-4bc6-8c2c-15b07cf8079c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 24
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "14b818b5-9948-4be1-82da-94d2fc60dd2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "82f61f19-973a-4d75-9ba6-175836b07e08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "57b1206f-c80c-4912-ac7c-8abf935c4cf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 24
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "d157a526-0d1d-4ab3-971f-dabf51153502",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4ce22734-686e-4aed-b277-d02596ba4381",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7958a8a9-1751-4307-aeed-2dbeeccc0c0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "79548076-5835-4594-8a8e-dd7bbc99fe72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "88ea8338-526e-451c-81ac-a357e719a858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "42321459-801f-4204-9834-66b3cf21c419",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "6a21670f-844d-45bb-95c0-10c93bd939a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 241,
                "y": 46
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "263cf159-6207-4a5b-9095-af4eabe04884",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 225,
                "y": 46
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c43dd280-f2ee-4589-81b3-3288789c124e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ab0c513a-3030-477c-a0cc-47289b0331b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 173,
                "y": 24
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f18f3b8d-3cff-46c5-9273-5409d6e909a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 40,
                "y": 68
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "73cfbd44-9b8c-495e-8031-8405128e3f32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 56,
                "y": 24
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f31dc45b-a8f5-4e20-8081-9c4baffc8444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 65,
                "y": 24
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "98bee994-0d37-43b4-9d65-327e4b346b05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 193,
                "y": 46
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "c006ae66-a1e7-4b1a-a96c-81d08ffca12d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 74,
                "y": 24
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a8998ded-d947-4f0a-9c1c-0b982ec844bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 47,
                "y": 24
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ed4c3326-cdab-4b26-a8b9-b0ece08d1435",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 233,
                "y": 46
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "90e8a695-34c0-433f-b12e-6387f68c762c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 83,
                "y": 24
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "7541dfb4-6165-4a6a-9fa3-5c48a82dd32b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 92,
                "y": 24
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "8ea3fe14-4f0a-4d6f-89a2-749fb5d996c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 70,
                "y": 68
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "48429ab1-0011-4c4e-b83e-92220f02bfeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 46,
                "y": 68
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "288815bb-9fca-427c-9c71-fe9ad8496a8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 101,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5d71590a-c143-4548-9de7-8814a2ff01d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 62,
                "y": 68
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "5e65649b-d30a-4dab-8b48-8ae7d8dee410",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a55ce444-b9ef-4bed-b925-23a44271e481",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 110,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f0327726-8736-4b00-9efc-b7e7b835f4b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 119,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "797465a0-e787-4618-81c9-235dadb6f3d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 128,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4150b5a6-2fee-43d0-b0ad-72372599428f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 137,
                "y": 24
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0df4543e-1065-46d5-8e73-88cba1d10056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 137,
                "y": 46
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "f30cc303-ff97-41d8-ba15-14ef3d865dc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 146,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9222247f-69c7-44df-af0d-aa8dee3ce356",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 22,
                "y": 68
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "262e8b7b-cabb-4746-a54b-f9f5a13454ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 155,
                "y": 24
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ae7c4138-720c-4514-9fad-85d196ebb053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c36b84dc-33a5-4b62-b6de-cadaaa74d9e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "67b24776-c706-473a-a6f3-c6e78decfdf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "58e5f0cf-f5fa-40bc-95b3-9b5ab60212cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 164,
                "y": 24
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "978d8598-0308-4413-b8b3-c83865064d4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 177,
                "y": 46
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "91c72466-31b8-4a1d-8d9f-00d3bcede600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d31f6d47-9aa4-4f56-8689-6fa2aafc1924",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 78,
                "y": 68
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b9ee1c48-5bbd-42bb-83ec-406318d89c61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 24
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "19feb85b-7517-4d1c-b32d-67b5dc387278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 191,
                "y": 24
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 22,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
{
    "id": "c3c3a757-f998-47dc-9466-1fe71ff9a906",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "room_20_path_2",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "c8f436df-0901-44e1-8060-224ad9124080",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 496,
            "y": 0,
            "speed": 100
        },
        {
            "id": "93f1b766-cca5-4555-b0c1-9763e0d0655b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 496,
            "y": 96,
            "speed": 100
        },
        {
            "id": "b7538f98-10a8-4619-b48b-812f7e13d8ef",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 96,
            "speed": 100
        },
        {
            "id": "005ba99b-b557-4d4b-8921-f940d9438bf2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 0,
            "speed": 100
        },
        {
            "id": "fb78b3f7-7f55-4e13-a37a-6678aa7b97d7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 496,
            "y": 0,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}
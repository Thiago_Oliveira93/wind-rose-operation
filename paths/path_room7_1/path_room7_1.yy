{
    "id": "6bfda9ab-a27f-49af-ae4c-6cb574240bbf",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_room7_1",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "4c0598f0-0161-4dac-982c-053e0b9c8b10",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 272,
            "y": 48,
            "speed": 100
        },
        {
            "id": "8238e884-b295-416c-82d5-f51a6b499f75",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 272,
            "y": 240,
            "speed": 100
        },
        {
            "id": "5355a4ac-a54b-4ff7-9f34-875fe8465f58",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 240,
            "speed": 100
        },
        {
            "id": "1d46ee10-9820-4c5f-a0ef-86f889dd095e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 48,
            "speed": 100
        },
        {
            "id": "a67e43f1-1f37-4e04-9289-543175587510",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 272,
            "y": 48,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}
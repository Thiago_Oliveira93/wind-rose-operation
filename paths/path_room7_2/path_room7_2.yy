{
    "id": "e0acccd2-c68e-4c16-a260-d37773a32058",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_room7_2",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "171fd0c4-2eec-4416-8735-4a586ab49160",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 464,
            "y": 240,
            "speed": 100
        },
        {
            "id": "5462d724-b8d4-464f-96a7-cda22d56ceb3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 464,
            "y": 48,
            "speed": 100
        },
        {
            "id": "50eb348e-5683-44fe-9576-a3ea08133483",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 272,
            "y": 48,
            "speed": 100
        },
        {
            "id": "0d8d5305-d871-4791-8f71-eaa0b53fe39a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 272,
            "y": 240,
            "speed": 100
        },
        {
            "id": "07b9b04e-1e48-4245-8ec0-eef0c8b5e56d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 464,
            "y": 240,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}
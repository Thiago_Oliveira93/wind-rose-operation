
{
    "name": "room_15",
    "id": "ef913df8-15ea-46fa-8aa7-b86a5e6791ff",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "64ae2a16-8733-411d-87b0-e68f9014f3ee",
        "93d4cd95-b783-421e-bd0f-b3e685f8e249",
        "c2d30e46-defc-404e-8a55-d1d9487c2488",
        "83a29604-85a6-48e9-9202-7c45ba5c1854",
        "0cddca89-e96f-4904-8933-8bf24bcb8b0f",
        "178446f0-f94e-43f6-b8ce-c836922bcdd0",
        "24b1fb72-0fb1-4d70-a8b1-bed52d7e9dc0",
        "5cc93ba0-8a6e-48d4-9b6e-b9a1c390aac3",
        "16ac5dcc-f08e-45ae-af3e-f621d4abdf06",
        "23a27157-8184-43f7-a68d-5dd52af40cd3",
        "916ade2b-ecc9-4bfb-a621-5adfe51c1c2c",
        "fa6c84b9-06ea-4f59-b0d9-089e2be15205",
        "551245a5-1e0b-4575-8a4a-5bf1e04356e5",
        "6701473f-6ccd-4a6c-b77e-7517065fa8bc",
        "42cab9de-0c62-472a-a727-a15d03471547",
        "c3ed2f71-2786-4858-b8ac-e44a7561c290",
        "f38282ef-b3f1-4901-bb27-a28a037d5a9d",
        "0af3902a-af99-48a5-aa34-c2607aaf9194",
        "4920fd3c-a47a-47a4-85e7-d632a372dc8c",
        "7070a655-9e5e-4253-97bf-10a619f7e2ca",
        "1ce80dfe-b4d9-40eb-ba19-6221d1ea5ed0",
        "571aa268-33c4-4fd4-9d17-9c1c0dfd2ce1",
        "53ae8be4-29b3-4c0e-96c1-7c3890284734",
        "56a52d3e-e6f1-4be4-88df-93c782e8bd99",
        "588ffa72-c70a-47fa-b85a-b015971097de",
        "5f246c82-ad2a-4338-b5b1-4bd9d590db59",
        "6ad6bbf0-9905-458f-82a7-0cfa4600892f",
        "bdf571be-e52f-4d90-b430-6e6e4ac84068",
        "138df006-5334-4180-85f8-91500d465ec3",
        "055ca9ca-e4db-4efe-a3ca-1688ce9f97b9",
        "9c92d50c-4382-4607-bfe9-34850766704e",
        "4ffec125-30bb-46c4-8a65-269fd36269b0",
        "bc953311-f2f9-4343-b930-83aba9ca7e54",
        "6e0d44a7-f329-4e7a-8b77-feac66eda786",
        "428c9688-590e-425c-a7e3-295b67ed459c",
        "6078b173-03fd-4694-8fbd-815a5c3cec16",
        "8cfa0894-305a-4137-b32d-b4e55b6e4d18",
        "8fd0dddd-5a7e-474e-b76c-dee3e9a0f4c7",
        "4624e3e2-b517-4a91-8aa9-19e392d01867",
        "dcde28be-b05f-4457-a307-54cba7644350",
        "e01705b2-7fe8-48cd-acc0-16a6b94ca657"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances_16",
            "id": "76e60c67-07fd-429e-b784-20a679e8e260",
            "depth": 0,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_1093C17C","id": "bc953311-f2f9-4343-b930-83aba9ca7e54","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_1093C17C.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1093C17C","objId": "2d54ba74-40a8-46a6-82a9-fe37c65aa3a5","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 496,"y": 48},
{"name": "inst_21E8FB04","id": "6e0d44a7-f329-4e7a-8b77-feac66eda786","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_21E8FB04.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_21E8FB04","objId": "2d54ba74-40a8-46a6-82a9-fe37c65aa3a5","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 336,"y": 144},
{"name": "inst_4B1BD8DB","id": "428c9688-590e-425c-a7e3-295b67ed459c","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_4B1BD8DB.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4B1BD8DB","objId": "2d54ba74-40a8-46a6-82a9-fe37c65aa3a5","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 144,"y": 272},
{"name": "inst_7E95F6C7","id": "6078b173-03fd-4694-8fbd-815a5c3cec16","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_7E95F6C7.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7E95F6C7","objId": "2d54ba74-40a8-46a6-82a9-fe37c65aa3a5","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 496,"y": 176}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "15651185-1d27-4727-9d86-2a768fe4a4be",
            "depth": 100,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_2BC67B2B","id": "178446f0-f94e-43f6-b8ce-c836922bcdd0","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2BC67B2B","objId": "2881b780-712f-4846-9c48-99c8210e5975","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 992,"y": 416},
{"name": "inst_5A3CB7A3","id": "24b1fb72-0fb1-4d70-a8b1-bed52d7e9dc0","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5A3CB7A3","objId": "a72be43a-bfbb-420f-8b5c-eb36ecd74298","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 96,"y": 352},
{"name": "inst_66FDBA8E","id": "64ae2a16-8733-411d-87b0-e68f9014f3ee","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_66FDBA8E","objId": "970e04d4-8106-4c4c-9479-f3a1922e0e12","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 0,"y": 160},
{"name": "inst_1335E926","id": "93d4cd95-b783-421e-bd0f-b3e685f8e249","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1335E926","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 18,"scaleY": 1,"mvc": "1.0","x": -32,"y": 288},
{"name": "inst_D1DB437","id": "c2d30e46-defc-404e-8a55-d1d9487c2488","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_D1DB437","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 9,"mvc": "1.0","x": 512,"y": 0},
{"name": "inst_393A5359","id": "83a29604-85a6-48e9-9202-7c45ba5c1854","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_393A5359","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 10,"mvc": "1.0","x": -32,"y": -32},
{"name": "inst_67740770","id": "0cddca89-e96f-4904-8933-8bf24bcb8b0f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_67740770","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 17,"scaleY": 1,"mvc": "1.0","x": 0,"y": -32},
{"name": "inst_B7063D","id": "5cc93ba0-8a6e-48d4-9b6e-b9a1c390aac3","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_B7063D.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_B7063D","objId": "2bcf7d05-f07f-4c95-a530-2360e9a701c8","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": -800,"y": 256},
{"name": "inst_2673BECC","id": "16ac5dcc-f08e-45ae-af3e-f621d4abdf06","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2673BECC","objId": "495e539e-26b7-4b11-8545-39f7b8ed327b","properties": null,"rotation": 40.47233,"scaleX": 1,"scaleY": 0,"mvc": "1.0","x": 0,"y": 112},
{"name": "inst_78343D3C","id": "23a27157-8184-43f7-a68d-5dd52af40cd3","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_78343D3C","objId": "9082d178-6ef9-4165-a244-6ae498803b64","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 32,"y": 352},
{"name": "inst_6AEBD583","id": "916ade2b-ecc9-4bfb-a621-5adfe51c1c2c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6AEBD583","objId": "495e539e-26b7-4b11-8545-39f7b8ed327b","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 64,"y": 352},
{"name": "inst_65250DF9","id": "fa6c84b9-06ea-4f59-b0d9-089e2be15205","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_65250DF9","objId": "2f4a35eb-b0e6-499b-aa64-a84f70bf6784","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 416,"y": 192},
{"name": "inst_497CFBB0","id": "551245a5-1e0b-4575-8a4a-5bf1e04356e5","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_497CFBB0","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 7,"mvc": "1.0","x": 160,"y": 64},
{"name": "inst_3EA4A34D","id": "6701473f-6ccd-4a6c-b77e-7517065fa8bc","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_3EA4A34D.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3EA4A34D","objId": "a81ebf8f-c3e1-4b0a-b883-7668d8b23366","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 0,"y": 352},
{"name": "inst_F3EADA5","id": "42cab9de-0c62-472a-a727-a15d03471547","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_F3EADA5","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 2,"scaleY": 2,"mvc": "1.0","x": 416,"y": 32},
{"name": "inst_6ABBA35C","id": "c3ed2f71-2786-4858-b8ac-e44a7561c290","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6ABBA35C","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 3,"scaleY": 1,"mvc": "1.0","x": 96,"y": 128},
{"name": "inst_4E4C2423","id": "f38282ef-b3f1-4901-bb27-a28a037d5a9d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4E4C2423","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 2,"mvc": "1.0","x": 96,"y": 224},
{"name": "inst_3F7930CA","id": "0af3902a-af99-48a5-aa34-c2607aaf9194","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3F7930CA","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 3,"scaleY": 1,"mvc": "1.0","x": 0,"y": 0},
{"name": "inst_5B57E5F0","id": "4920fd3c-a47a-47a4-85e7-d632a372dc8c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5B57E5F0","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 2,"mvc": "1.0","x": 0,"y": 32},
{"name": "inst_6C55DEBB","id": "7070a655-9e5e-4253-97bf-10a619f7e2ca","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6C55DEBB","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 2,"scaleY": 2,"mvc": "1.0","x": 64,"y": 64},
{"name": "inst_2609730A","id": "1ce80dfe-b4d9-40eb-ba19-6221d1ea5ed0","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_2609730A.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2609730A","objId": "2bcf7d05-f07f-4c95-a530-2360e9a701c8","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 192,"y": 256},
{"name": "inst_42A37510","id": "571aa268-33c4-4fd4-9d17-9c1c0dfd2ce1","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_42A37510","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 2,"mvc": "1.0","x": 32,"y": 160},
{"name": "inst_764BBA83","id": "53ae8be4-29b3-4c0e-96c1-7c3890284734","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_764BBA83","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 0,"y": 256},
{"name": "inst_FD16C0B","id": "56a52d3e-e6f1-4be4-88df-93c782e8bd99","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_FD16C0B","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 224,"y": 32},
{"name": "inst_59554411","id": "588ffa72-c70a-47fa-b85a-b015971097de","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_59554411","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 224,"y": 160},
{"name": "inst_49EB5F6B","id": "5f246c82-ad2a-4338-b5b1-4bd9d590db59","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_49EB5F6B","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 3,"mvc": "1.0","x": 256,"y": 96},
{"name": "inst_2739A36","id": "6ad6bbf0-9905-458f-82a7-0cfa4600892f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2739A36","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 2,"scaleY": 1,"mvc": "1.0","x": 288,"y": 96},
{"name": "inst_4AC327B7","id": "bdf571be-e52f-4d90-b430-6e6e4ac84068","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4AC327B7","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 7,"mvc": "1.0","x": 352,"y": 64},
{"name": "inst_4F09001D","id": "138df006-5334-4180-85f8-91500d465ec3","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4F09001D","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 2,"mvc": "1.0","x": 256,"y": 224},
{"name": "inst_4415229C","id": "055ca9ca-e4db-4efe-a3ca-1688ce9f97b9","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4415229C","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 2,"mvc": "1.0","x": 288,"y": 0},
{"name": "inst_31751883","id": "9c92d50c-4382-4607-bfe9-34850766704e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_31751883","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 320,"y": 0},
{"name": "inst_6D5D321A","id": "4ffec125-30bb-46c4-8a65-269fd36269b0","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6D5D321A","objId": "6578cf46-3ce1-4f77-9e31-1c6b90003230","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 224,"y": 64},
{"name": "inst_84A6B7D","id": "8cfa0894-305a-4137-b32d-b4e55b6e4d18","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_84A6B7D","objId": "01b8fc5a-ff3b-4971-a75e-32134982295d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 224,"y": 96},
{"name": "inst_3678F156","id": "8fd0dddd-5a7e-474e-b76c-dee3e9a0f4c7","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3678F156","objId": "89b43b19-6503-4633-83fd-8ed10919aecd","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 32,"y": 224},
{"name": "inst_3E792657","id": "4624e3e2-b517-4a91-8aa9-19e392d01867","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3E792657","objId": "e27bd563-207d-456a-bc10-d5c65fd3d2ed","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 224,"y": 0},
{"name": "inst_7E61DF16","id": "dcde28be-b05f-4457-a307-54cba7644350","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7E61DF16","objId": "e27bd563-207d-456a-bc10-d5c65fd3d2ed","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 240,"y": 16},
{"name": "inst_F329E53","id": "e01705b2-7fe8-48cd-acc0-16a6b94ca657","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_F329E53","objId": "89b43b19-6503-4633-83fd-8ed10919aecd","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 0,"y": 96}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Tiles_1",
            "id": "2a3a0b5d-a6a6-4a0d-b55d-709e1a1e80ff",
            "depth": 200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRTileLayer",
            "prev_tileheight": 16,
            "prev_tilewidth": 16,
            "mvc": "1.0",
            "tiles": {
                "SerialiseData": null,
                "SerialiseHeight": 18,
                "SerialiseWidth": 32,
                "TileSerialiseData": [
                    96,97,97,97,97,98,186,186,186,186,186,186,186,186,186,186,186,186,97,97,97,97,187,187,187,187,187,187,187,187,187,187,
                    96,98,167,167,167,167,206,206,206,206,206,206,206,206,206,206,206,206,97,97,168,169,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    96,98,186,186,186,186,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,76,78,2147483648,2147483648,97,97,188,189,2147483648,2147483648,2147483648,2147483648,76,77,77,78,2147483648,2147483648,
                    96,98,206,206,206,206,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,116,118,2147483648,2147483648,167,167,208,209,2147483648,2147483648,2147483648,2147483648,96,97,97,98,2147483648,2147483648,
                    117,118,2147483648,2147483648,76,77,77,78,2147483648,2147483648,76,78,2147483648,2147483648,188,189,2147483648,2147483648,186,186,2147483648,2147483648,76,78,2147483648,2147483648,96,97,97,98,2147483648,2147483648,
                    169,170,2147483648,2147483648,96,97,97,98,2147483648,2147483648,96,98,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,206,206,2147483648,2147483648,96,98,2147483648,2147483648,168,168,168,168,2147483648,2147483648,
                    186,186,2147483648,2147483648,96,97,97,98,2147483648,2147483648,96,98,2147483648,2147483648,76,77,77,77,77,77,77,77,97,98,2147483648,2147483648,188,188,188,188,2147483648,2147483648,
                    206,206,2147483648,2147483648,167,167,96,98,2147483648,2147483648,96,98,2147483648,2147483648,167,167,96,98,167,167,167,167,96,98,2147483648,2147483648,208,208,208,208,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,186,186,96,97,77,77,97,98,2147483648,2147483648,186,186,96,98,186,186,186,186,96,98,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,206,206,167,168,169,170,96,98,2147483648,2147483648,206,206,96,98,206,206,206,206,96,98,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,76,78,2147483648,2147483648,186,186,186,186,96,98,2147483648,2147483648,76,77,97,98,2147483648,2147483648,2147483648,2147483648,96,98,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,96,98,2147483648,2147483648,206,206,206,206,96,98,2147483648,2147483648,167,167,167,167,2147483648,2147483648,2147483648,2147483648,96,98,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,116,118,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,96,98,2147483648,2147483648,188,188,188,188,2147483648,2147483648,2147483648,2147483648,96,98,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,167,167,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,96,98,2147483648,2147483648,208,208,208,208,2147483648,2147483648,2147483648,2147483648,96,98,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,186,186,2147483648,2147483648,76,78,2147483648,2147483648,96,98,2147483648,2147483648,2147483648,2147483648,76,78,2147483648,2147483648,2147483648,2147483648,96,98,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,206,206,2147483648,2147483648,96,98,2147483648,2147483648,96,98,2147483648,2147483648,2147483648,2147483648,96,98,2147483648,2147483648,2147483648,2147483648,96,98,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    76,78,2147483648,2147483648,2147483648,2147483648,96,98,2147483648,2147483648,96,98,2147483648,2147483648,2147483648,2147483648,96,98,2147483648,2147483648,2147483648,2147483648,96,98,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    97,98,2147483648,2147483648,2147483648,2147483648,96,98,2147483648,2147483648,96,98,2147483648,2147483648,2147483648,2147483648,96,98,2147483648,2147483648,2147483648,2147483648,96,98,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648
                ]
            },
            "tilesetId": "20de00a5-73c7-4832-a535-d29eff45e184",
            "userdefined_depth": false,
            "visible": true,
            "x": 0,
            "y": 0
        },
        {
            "__type": "GMRPathLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Path_1",
            "id": "a4cbc8d9-1806-4d80-b156-32e05b87a4f6",
            "colour": { "Value": 4278190335 },
            "depth": 300,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [
                {
                    "__type": "GMRPathLayer_Model:#YoYoStudio.MVCFormat",
                    "name": "Path_3",
                    "id": "d6df88fc-7174-48df-8d94-6fa108749e19",
                    "colour": { "Value": 4278190335 },
                    "depth": 400,
                    "grid_x": 16,
                    "grid_y": 16,
                    "hierarchyFrozen": false,
                    "hierarchyVisible": true,
                    "inheritLayerDepth": false,
                    "inheritLayerSettings": false,
                    "inheritSubLayers": false,
                    "inheritVisibility": false,
                    "layers": [

                    ],
                    "m_parentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "modelName": "GMRPathLayer",
                    "pathId": "84ea5ced-8b1c-4d13-983b-2225d22d6d61",
                    "mvc": "1.0",
                    "userdefined_depth": false,
                    "visible": true
                },
                {
                    "__type": "GMRPathLayer_Model:#YoYoStudio.MVCFormat",
                    "name": "Path_2",
                    "id": "71b2be2d-2b31-4b02-a1c9-e7c1c01e411c",
                    "colour": { "Value": 4278190335 },
                    "depth": 500,
                    "grid_x": 16,
                    "grid_y": 16,
                    "hierarchyFrozen": false,
                    "hierarchyVisible": true,
                    "inheritLayerDepth": false,
                    "inheritLayerSettings": false,
                    "inheritSubLayers": false,
                    "inheritVisibility": false,
                    "layers": [

                    ],
                    "m_parentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "modelName": "GMRPathLayer",
                    "pathId": "504fab36-df3f-4ac7-98bb-4e4bbc864b54",
                    "mvc": "1.0",
                    "userdefined_depth": false,
                    "visible": true
                }
            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRPathLayer",
            "pathId": "055ee038-332e-4df6-9db0-6494c99ff313",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "2d2f7293-0075-4f7d-b96a-d7fd741a1bbe",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 600,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "cf9639e6-d49c-480f-996d-0f06ebfd0aea",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": true,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "c6b4a6dd-2ddc-4cc2-babe-f2d3551b983d",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "8aa5abfa-7e25-4898-93bc-dd6887df1c96",
        "Height": 288,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 512
    },
    "mvc": "1.0",
    "views": [
{"id": "aea4b784-f600-45c7-9405-902b16eb64d5","hborder": 400,"hport": 450,"hspeed": -1,"hview": 288,"inherit": false,"modelName": "GMRView","objId": "970e04d4-8106-4c4c-9479-f3a1922e0e12","mvc": "1.0","vborder": 260,"visible": true,"vspeed": -1,"wport": 800,"wview": 512,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "ccdee270-8aa1-4793-8a98-6b560cd2fd3e","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "88c8366e-5b20-40fc-91ee-5a0bc150ea29","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "4e47b40b-8c15-482b-8164-fc70ec548e9c","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "b799d8c5-3646-41af-a78f-5ddefaf2e390","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "08a7ff03-ea69-415e-8604-24939a3b698b","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "6294e595-8cbf-454e-bde0-ae0c4dbaf053","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "2c77bc77-ce68-46a8-919b-6014d3c7503d","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "bb73ef9b-58b3-4478-a1cd-fa6f5b6c9a60",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}
/// @description Insert description here
///
draw_self()
draw_set_font(font_default)
draw_set_color(c_red)
draw_text(x,y+32,instance_number(obj_timebomb_turn))

if turn_based
{
	draw_text(x,y,turns)
}
else
{
	draw_text(x,y,time/60)
}

{
    "id": "2d54ba74-40a8-46a6-82a9-fe37c65aa3a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_timebomb_turn",
    "eventList": [
        {
            "id": "f08520a1-b0ee-47a3-ab78-a496357b2309",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2d54ba74-40a8-46a6-82a9-fe37c65aa3a5"
        },
        {
            "id": "37161051-f2fa-4d02-a0be-852b18f19162",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2d54ba74-40a8-46a6-82a9-fe37c65aa3a5"
        },
        {
            "id": "da9b3719-8698-419e-871f-365c6db415ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2d54ba74-40a8-46a6-82a9-fe37c65aa3a5"
        },
        {
            "id": "d7a227fc-2766-4d39-9a3c-6dc99c590970",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "970e04d4-8106-4c4c-9479-f3a1922e0e12",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2d54ba74-40a8-46a6-82a9-fe37c65aa3a5"
        },
        {
            "id": "1c76713e-0a13-420d-b811-4e6e45583555",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "2d54ba74-40a8-46a6-82a9-fe37c65aa3a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5a28d5ca-f3f7-4c9d-b15b-dd37fcc6672c",
    "visible": true
}
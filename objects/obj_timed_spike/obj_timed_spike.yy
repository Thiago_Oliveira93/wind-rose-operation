{
    "id": "dc0da799-a866-471b-9163-680691876e0b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_timed_spike",
    "eventList": [
        {
            "id": "05cea873-7527-46b7-bcde-9ccaa8ad021f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dc0da799-a866-471b-9163-680691876e0b"
        },
        {
            "id": "b5dc2685-00e2-4cec-a060-6b490392e6f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dc0da799-a866-471b-9163-680691876e0b"
        },
        {
            "id": "b1c60e07-72ba-4c95-a36a-0a3ffb68b83c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "dc0da799-a866-471b-9163-680691876e0b"
        },
        {
            "id": "48f54868-dd93-45ce-80b5-6c6694178cfe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dc0da799-a866-471b-9163-680691876e0b"
        },
        {
            "id": "beab6a53-f853-42fc-a5c4-67386c8e148f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "970e04d4-8106-4c4c-9479-f3a1922e0e12",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "dc0da799-a866-471b-9163-680691876e0b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b36916fd-0958-4ed8-bf22-ededdfe0cbdd",
    "visible": true
}
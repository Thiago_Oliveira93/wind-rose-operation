{
    "id": "2006f6b8-d00a-4f2c-8054-c1eb9446f8f6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_direction_changer_left",
    "eventList": [
        {
            "id": "9b23d132-0272-493e-a928-23bcc1bce6e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2006f6b8-d00a-4f2c-8054-c1eb9446f8f6"
        },
        {
            "id": "81c690d3-b4a6-4dea-908c-df62b81c1285",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2006f6b8-d00a-4f2c-8054-c1eb9446f8f6"
        },
        {
            "id": "b7735a61-bead-4276-b3b5-188a121abc01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2006f6b8-d00a-4f2c-8054-c1eb9446f8f6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "94b501f2-360c-445d-9e9c-c510f4a31de7",
    "visible": true
}
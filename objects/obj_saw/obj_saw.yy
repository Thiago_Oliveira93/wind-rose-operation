{
    "id": "83301ab0-20a2-4db3-8791-b39e2ba723c6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_saw",
    "eventList": [
        {
            "id": "c9f3b553-2c8e-4d88-b8f7-54b215a9002c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "83301ab0-20a2-4db3-8791-b39e2ba723c6"
        },
        {
            "id": "c5a6d268-8987-4118-a0a6-926eee670cbc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "83301ab0-20a2-4db3-8791-b39e2ba723c6"
        },
        {
            "id": "d5cbc1d3-32ed-4b4f-b9f0-a81d72fc4037",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "83301ab0-20a2-4db3-8791-b39e2ba723c6"
        },
        {
            "id": "6301116c-f35c-4c52-afa8-b3acb25fea0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "83301ab0-20a2-4db3-8791-b39e2ba723c6"
        },
        {
            "id": "e5729c49-2995-4c15-9919-0d578e7135bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "83301ab0-20a2-4db3-8791-b39e2ba723c6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d7321c18-59ce-48c4-9ac4-56e5fc7cd0e6",
    "visible": true
}
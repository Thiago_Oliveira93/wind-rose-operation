{
    "id": "7d3366d1-b2fb-48b4-bcfc-14eb68e574f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_zelda_room_transition_down",
    "eventList": [
        {
            "id": "2e6fdfd7-667c-4029-8a7a-de54c195413e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7d3366d1-b2fb-48b4-bcfc-14eb68e574f1"
        },
        {
            "id": "1e3c1436-ea9d-4a8d-85db-4bc18126feef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7d3366d1-b2fb-48b4-bcfc-14eb68e574f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b36916fd-0958-4ed8-bf22-ededdfe0cbdd",
    "visible": true
}
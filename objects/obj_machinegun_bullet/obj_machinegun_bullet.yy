{
    "id": "8dd6467f-6779-44f4-b238-65f0b4240c1b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_machinegun_bullet",
    "eventList": [
        {
            "id": "6c6f6e65-c22a-4b45-88fb-7f0a5cf7ac0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8dd6467f-6779-44f4-b238-65f0b4240c1b"
        },
        {
            "id": "564d4e21-91d8-4135-8b39-72526fa083c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8dd6467f-6779-44f4-b238-65f0b4240c1b"
        },
        {
            "id": "a607cc16-e10a-477e-b61f-5c281d38e833",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "01b8fc5a-ff3b-4971-a75e-32134982295d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8dd6467f-6779-44f4-b238-65f0b4240c1b"
        },
        {
            "id": "61b8567b-d918-4413-b24d-ef0f8d6b89f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8dd6467f-6779-44f4-b238-65f0b4240c1b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "63bed941-43e8-4871-a34c-bb35b7a1fe83",
    "visible": true
}
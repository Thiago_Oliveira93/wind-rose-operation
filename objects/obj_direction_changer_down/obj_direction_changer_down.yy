{
    "id": "8878ce2d-7ce6-42d9-aabf-af2582308186",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_direction_changer_down",
    "eventList": [
        {
            "id": "11886074-bdd5-46da-a810-87c80fed74b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8878ce2d-7ce6-42d9-aabf-af2582308186"
        },
        {
            "id": "f6504a06-1192-4c0e-a1d3-789b6399b97c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8878ce2d-7ce6-42d9-aabf-af2582308186"
        },
        {
            "id": "ebb5d8fa-5960-4a4a-a72c-9e8f356fdbd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8878ce2d-7ce6-42d9-aabf-af2582308186"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f991fef3-00c5-47ab-86e4-6404c581b671",
    "visible": true
}
{
    "id": "c4bee5de-89d8-4834-bdf9-3eee7db75794",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_box",
    "eventList": [
        {
            "id": "c816e7d5-e667-4961-874f-896767d68d66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c4bee5de-89d8-4834-bdf9-3eee7db75794"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "668d061c-336f-4a04-b67f-74d6a1ab0235",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "88331037-c045-41b8-9c45-f7fb4d8d3f6f",
    "visible": true
}
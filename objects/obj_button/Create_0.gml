/// @description Insert description here
///
depth = obj_player.depth+1
my_solid1 = instance_create_depth(x+32,y+32,depth,obj_direction_stopper)
my_solid2 = instance_create_depth(x-32,y+32,depth,obj_direction_stopper)
my_solid3 = instance_create_depth(x+32,y-32,depth,obj_direction_stopper)
my_solid4 = instance_create_depth(x-32,y-32,depth,obj_direction_stopper)
activated = false
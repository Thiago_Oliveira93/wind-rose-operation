{
    "id": "89b43b19-6503-4633-83fd-8ed10919aecd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button",
    "eventList": [
        {
            "id": "0b599259-dfbf-49bd-8493-022403eb34eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89b43b19-6503-4633-83fd-8ed10919aecd"
        },
        {
            "id": "2ee1e878-8d7c-41d7-bacd-dc64ea700d96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "89b43b19-6503-4633-83fd-8ed10919aecd"
        },
        {
            "id": "faec6ede-71ef-487f-983a-edfc5e65042a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "89b43b19-6503-4633-83fd-8ed10919aecd"
        },
        {
            "id": "fee65b1c-267f-4817-ae81-aee0f14e0b9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "970e04d4-8106-4c4c-9479-f3a1922e0e12",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "89b43b19-6503-4633-83fd-8ed10919aecd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "def6d93e-0c93-4cb5-a0f6-597cb8446f6d",
    "visible": true
}
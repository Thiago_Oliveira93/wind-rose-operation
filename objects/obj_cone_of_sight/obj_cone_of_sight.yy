{
    "id": "5ff7e33c-d378-4015-8662-a470cc697bf5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cone_of_sight",
    "eventList": [
        {
            "id": "0565a251-0d3d-43fc-a634-220c355a9451",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5ff7e33c-d378-4015-8662-a470cc697bf5"
        },
        {
            "id": "9d6614e7-0e5c-44c4-9a10-3934dbd62e21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5ff7e33c-d378-4015-8662-a470cc697bf5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e231ff35-c99e-49b3-ab27-68803eac3eab",
    "visible": true
}
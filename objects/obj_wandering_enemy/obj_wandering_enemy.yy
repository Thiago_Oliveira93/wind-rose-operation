{
    "id": "ea3ec9ad-6edd-4509-83a9-7ed7cf7712b8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wandering_enemy",
    "eventList": [
        {
            "id": "305440c8-f92b-4e36-9ae8-17133a6b611f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ea3ec9ad-6edd-4509-83a9-7ed7cf7712b8"
        },
        {
            "id": "9b56454a-5505-422b-ae00-e5e53c8eb749",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ea3ec9ad-6edd-4509-83a9-7ed7cf7712b8"
        },
        {
            "id": "fb29da24-2e63-40eb-b04f-9063c5efccd0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ea3ec9ad-6edd-4509-83a9-7ed7cf7712b8"
        },
        {
            "id": "92cb0089-4283-4c63-ae23-9881309f7e3a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ea3ec9ad-6edd-4509-83a9-7ed7cf7712b8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "08a80468-2c77-4a3a-a75a-adfebb97fe97",
    "visible": true
}
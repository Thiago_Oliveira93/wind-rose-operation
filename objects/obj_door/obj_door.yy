{
    "id": "be32cc22-ab08-4cf7-853e-3ba8d7bcb6a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_door",
    "eventList": [
        {
            "id": "ad548704-66c1-42f3-b63a-82c151e4d85d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "be32cc22-ab08-4cf7-853e-3ba8d7bcb6a9"
        },
        {
            "id": "694b5905-911b-4285-98b5-717c1a3cbc08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "be32cc22-ab08-4cf7-853e-3ba8d7bcb6a9"
        },
        {
            "id": "abd426b4-64f9-4b4a-9551-7f58e3f3ac71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "be32cc22-ab08-4cf7-853e-3ba8d7bcb6a9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ce89e17d-5f9b-4b87-89dc-db4c1c00ec2d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d60cc1b6-69b2-4c4e-9e73-5e7aa7eda25f",
    "visible": true
}
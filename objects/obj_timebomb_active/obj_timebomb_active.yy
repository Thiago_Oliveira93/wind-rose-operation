{
    "id": "3f5d8f85-ade9-4c60-94a4-f3cac29440d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_timebomb_active",
    "eventList": [
        {
            "id": "f000491a-98b3-43a0-901d-4d72fb3341f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3f5d8f85-ade9-4c60-94a4-f3cac29440d3"
        },
        {
            "id": "f412d9b1-321b-469c-8d1a-6a4f7c00d011",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3f5d8f85-ade9-4c60-94a4-f3cac29440d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5a28d5ca-f3f7-4c9d-b15b-dd37fcc6672c",
    "visible": true
}
{
    "id": "3203835b-1298-4903-8c27-77455acc5da1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_card",
    "eventList": [
        {
            "id": "fd8d73c9-b106-495a-97ad-58f48081fc22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3203835b-1298-4903-8c27-77455acc5da1"
        },
        {
            "id": "405d604e-e454-4fc1-ae34-6f5c86051175",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3203835b-1298-4903-8c27-77455acc5da1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "191e4142-829d-41fd-9a7f-a7500f620577",
    "visible": true
}
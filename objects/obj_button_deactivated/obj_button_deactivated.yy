{
    "id": "89b43b19-6503-4633-83fd-8ed10919aecd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_deactivated",
    "eventList": [
        {
            "id": "ac4402d2-e19b-478d-bd97-c67f856391d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89b43b19-6503-4633-83fd-8ed10919aecd"
        },
        {
            "id": "2bbc12bb-f47a-4267-a046-e26c32405844",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "970e04d4-8106-4c4c-9479-f3a1922e0e12",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "89b43b19-6503-4633-83fd-8ed10919aecd"
        },
        {
            "id": "ed6d15f9-3c31-4232-a0d8-e6bd8cc055e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "89b43b19-6503-4633-83fd-8ed10919aecd"
        },
        {
            "id": "63aa292c-8b76-4cf8-9d3d-989592c0532d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "89b43b19-6503-4633-83fd-8ed10919aecd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "668d061c-336f-4a04-b67f-74d6a1ab0235",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "501231e6-bc15-4385-b055-f1da787e1e09",
    "visible": true
}
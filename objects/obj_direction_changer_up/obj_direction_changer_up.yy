{
    "id": "1c645a29-5158-49d4-a74c-951b99bb269b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_direction_changer_up",
    "eventList": [
        {
            "id": "beb055b7-9ca9-4c27-a5de-1278943d94d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1c645a29-5158-49d4-a74c-951b99bb269b"
        },
        {
            "id": "eafa0210-15fb-4bef-bb5d-2000c32668e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1c645a29-5158-49d4-a74c-951b99bb269b"
        },
        {
            "id": "9fa2d332-075a-4c68-ba46-e29767fd13ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1c645a29-5158-49d4-a74c-951b99bb269b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b4a33e19-d7d0-405c-ab96-b696dc2af9aa",
    "visible": true
}
{
    "id": "6578cf46-3ce1-4f77-9e31-1c6b90003230",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_spike",
    "eventList": [
        {
            "id": "92160193-694b-4ce7-9237-9456bd3734b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "970e04d4-8106-4c4c-9479-f3a1922e0e12",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6578cf46-3ce1-4f77-9e31-1c6b90003230"
        },
        {
            "id": "0a7d498f-e1d7-4b83-814c-04376fdc9b6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6578cf46-3ce1-4f77-9e31-1c6b90003230"
        },
        {
            "id": "1a7343eb-2b60-4e30-ac9f-d0ac883c3ef8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6578cf46-3ce1-4f77-9e31-1c6b90003230"
        },
        {
            "id": "bd4228e2-410c-4778-8a41-f0603cc096fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6578cf46-3ce1-4f77-9e31-1c6b90003230"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8f1ee375-f3e7-4ba5-a4a3-176c79921811",
    "visible": true
}
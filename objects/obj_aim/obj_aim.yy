{
    "id": "d260e500-e29c-47e5-8f1a-ef0fd56446ee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_aim",
    "eventList": [
        {
            "id": "0d62ad9a-8ff7-45b6-bc6e-558528defad9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d260e500-e29c-47e5-8f1a-ef0fd56446ee"
        },
        {
            "id": "9bcdb658-f563-4783-aa01-4ac991952b0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d260e500-e29c-47e5-8f1a-ef0fd56446ee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d81e3d6-ca0c-4995-b2b3-509c312e3926",
    "visible": true
}
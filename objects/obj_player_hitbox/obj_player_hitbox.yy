{
    "id": "c95653b5-1fe1-4624-9b5b-b36e9f68b412",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_hitbox",
    "eventList": [
        {
            "id": "14e335e4-2e1d-400c-8e60-208b34e015c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c95653b5-1fe1-4624-9b5b-b36e9f68b412"
        },
        {
            "id": "d2accca3-0b4e-480f-b4c8-d013877fcf35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c95653b5-1fe1-4624-9b5b-b36e9f68b412"
        },
        {
            "id": "1bf548fc-a6d8-4b34-84ad-822a5fb0269e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c95653b5-1fe1-4624-9b5b-b36e9f68b412"
        },
        {
            "id": "5c3bf526-432a-49bd-a9cf-f6933851fa14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2bcf7d05-f07f-4c95-a530-2360e9a701c8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c95653b5-1fe1-4624-9b5b-b36e9f68b412"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9d859fe-5198-4274-90e6-886c4387bf70",
    "visible": true
}
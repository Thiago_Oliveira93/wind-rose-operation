{
    "id": "970e04d4-8106-4c4c-9479-f3a1922e0e12",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "8c6c1f96-edee-49c3-92f9-bd7848a6c3c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "970e04d4-8106-4c4c-9479-f3a1922e0e12"
        },
        {
            "id": "2877e1ae-0b11-4dc0-bb1e-e6b4511716e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "970e04d4-8106-4c4c-9479-f3a1922e0e12"
        },
        {
            "id": "2c498f08-5a3a-49a1-98e0-082299871bab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "970e04d4-8106-4c4c-9479-f3a1922e0e12"
        },
        {
            "id": "28794724-09fb-4d18-9a3b-ef339fdf8cdc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "970e04d4-8106-4c4c-9479-f3a1922e0e12"
        },
        {
            "id": "01b4a931-5498-4ab4-bfd2-25743984c433",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5ff7e33c-d378-4015-8662-a470cc697bf5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "970e04d4-8106-4c4c-9479-f3a1922e0e12"
        },
        {
            "id": "632ac2ef-f06f-4e79-bbb0-785db222cb20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "970e04d4-8106-4c4c-9479-f3a1922e0e12"
        },
        {
            "id": "985bcaa8-7638-4645-8b14-27ad85e2337c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8dd6467f-6779-44f4-b238-65f0b4240c1b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "970e04d4-8106-4c4c-9479-f3a1922e0e12"
        },
        {
            "id": "862f5eef-ed29-48e7-8670-d8f417d62b6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3203835b-1298-4903-8c27-77455acc5da1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "970e04d4-8106-4c4c-9479-f3a1922e0e12"
        }
    ],
    "maskSpriteId": "08a80468-2c77-4a3a-a75a-adfebb97fe97",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "418aa879-bdc5-4ab9-a2b9-0f32d580d160",
    "visible": true
}
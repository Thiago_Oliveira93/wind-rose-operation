/// @description Player Behavior
///

scr_move_and_collide()
scr_get_input()



//Moving state

if hsp!=0 or vsp !=0
{
	moving = true
}
else
{
	moving = false
}

///aim
if !moving
{
	if k_aim
	{
		
		my_aim_angle = point_direction(x+32,y+32,mouse_x,mouse_y)
		if gun_time_hits>0
		{
			if shoot_
			{
				instance_create_layer(x+32,y+32,"Instances",obj_player_bullet)
				gun_time_hits-=1
			}
		}
	}
	//	number_of_circle_collisions+=1
	//	if number_of_circle_collisions == 3
	//	{
	//		 my_aim = instance_create_layer(x+32,y+32,"Instances",obj_aim)
	//	}
		
	//}
	//else
	//{
	//	if instance_exists(my_aim)
	//	{
	//		instance_destroy(my_aim)
	//		number_of_circle_collisions = 0
	//	}
	//}
}








///Right move
if up == 0 and down == 0 and left == 0
{
	if Kright
	{
		right = true
		//close_wall_direction = 5
	}

	if right
	{
		sprite_index = spr_walk_right
		if sprite_index != spr_close_wall_right
		image_speed = 1
		hsp = movespeed
		Kup = false
		Kleft = false
		Kdown = false
		
	}

	if place_meeting(x+movespeed,y,obj_solid)
	{
		right = false
		//close_wall_direction = 1
	}
}

///Up Move
if right == 0 and down == 0 and left == 0
{
	if Kup
	{
		up = true
		//close_wall_direction = 5
	}

	if up
	{
		sprite_index = spr_walk_up
		image_speed = 1
		vsp = -movespeed
		
		Kleft = false
		Kdown = false
		Kright = false
	}

	if place_meeting(x,y-movespeed,obj_solid)
	{
		up = false
		//close_wall_direction = 0
	}
}

///Left Move
if right == 0 and down == 0 and up == 0
{
	if Kleft
	{
		left = true
		//close_wall_direction = 5
	}

	if left
	{
		sprite_index = spr_walk_left
		if sprite_index != spr_close_wall_left
		image_speed = 1
		hsp = -movespeed
		Kup = false
		Kdown = false
		Kright = false
	}

	if place_meeting(x-movespeed,y,obj_solid)
	{
		left = false
		
		//close_wall_direction = 3
		
	}
}

///Down Move
if right == 0 and left == 0 and up == 0
{
	if Kdown
	{
		down = true
		//close_wall_direction = 5
	}

	if down
	{
		sprite_index = spr_walk_down
		image_speed = 1
		vsp = movespeed
		Kup = false
		Kleft = false
		Kright = false
	}

	if place_meeting(x,y+movespeed,obj_solid)
	{
		down = false
		//close_wall_direction = 2
		
	}
}

scr_player_wall_sprites()


///Direction change collision
var block_changer_right = instance_place(x,y,obj_direction_changer_right)
if place_meeting(x,y,obj_direction_changer_right)
{
	if y == block_changer_right.y //or x == block_changer.x
	{
			up = 0
			right = 1
			left = 0
			down = 0
			vsp = 0
			instance_destroy(block_changer_right)
	}
}


var block_changer_left = instance_place(x,y,obj_direction_changer_left)
if place_meeting(x,y,obj_direction_changer_left)
{
	if y == block_changer_left.y //or x == block_changer.x
	{
			up = 0
			right = 0
			left = 1
			down = 0
			vsp = 0
			instance_destroy(block_changer_left)
	}
}

var block_changer_up = instance_place(x,y,obj_direction_changer_up)
if place_meeting(x,y,obj_direction_changer_up)
{
	if x == block_changer_up.x //or x == block_changer.x
	{
			up = 1
			right = 0
			left = 0
			down = 0
			hsp = 0
			instance_destroy(block_changer_up)
	}
}

var block_changer_down = instance_place(x,y,obj_direction_changer_down)
if place_meeting(x,y,obj_direction_changer_down)
{
	if x == block_changer_down.x //or x == block_changer.x
	{
			up = 0
			right = 0
			left = 0
			down = 1
			hsp = 0
			instance_destroy(block_changer_down)
	}
}

var block_changer_stop = instance_place(x,y,obj_direction_stopper)
if place_meeting(x,y,obj_direction_stopper)
{
	if x == block_changer_stop.x and y == block_changer_stop.y
	{
			up = 0
			right = 0
			left = 0
			down = 0
			hsp = 0
			vsp = 0
			instance_destroy(block_changer_stop)
	}
}


//Camouflage stuff
if invisible_enable
{
	if k_invisible and stamina>0
	{
		visible_ = false
	}

	else
	{
		visible_ = true
	}

	if k_invisible
	{
		if moving
		{
			
		
			if stamina> 0
			{
				stamina -=0.5
				image_alpha = 0.5
			}
		}
	}
	else
	{

		image_alpha = 1
		{
			if stamina<max_stamina
			{
				stamina+=0.01
			}
		}
	
	}
}


///Melee attack
if sword_enable
{
	if moving
	{
		if shoot_ and hit_sword>0
		{
			hit_sword-=1
			instance_create_layer(x+32,y+32,"Instances",obj_player_hitbox)
		
		}
	}
}

///Make the time walk when the player is stopped
if Kadvance
{
	stamina-=0.1
}
if moving
{
	if stamina<max_stamina
	{
		stamina+=0.05
	}
}


///Turns to timebomb





/// @description Insert description here
///
sprite_index = spr_idle
hsp = 0
vsp = 0
movespeed =4
deact_keys = false
visible_ = true
moving = false

block_destroy = noone
card_id = noone

///direction variables
up = 0
down = 0
left = 0
right = 0


//dealing with targets
my_targets = ds_list_create()
number_of_circle_collisions = 0
my_aim = noone

///powerup stuff

block_enable = false
sword_enable = false
invisible_enable = false
shoot_enable = false

///attack properties

max_stamina = 20
stamina = max_stamina
max_sword_hit = 2
hit_sword = max_sword_hit


///invisible properties

max_invisible = 3
invisible_hits = max_invisible


///timebomturnbased
turn = false


///blocks properties

max_blocks = 5
blocks_hits = max_blocks


///close to wall control
close_wall_direction = noone


///aim control
my_aim_angle = noone
gun_time_hits = 0


{
    "id": "a72be43a-bfbb-420f-8b5c-eb36ecd74298",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_grid",
    "eventList": [
        {
            "id": "678c6b8d-f581-47d8-bb48-96954a345f20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a72be43a-bfbb-420f-8b5c-eb36ecd74298"
        },
        {
            "id": "c902a4f6-6892-49e2-8c6d-eb59a84caea6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "a72be43a-bfbb-420f-8b5c-eb36ecd74298"
        },
        {
            "id": "99b9bad0-cd7b-4922-b28d-77dad163f979",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a72be43a-bfbb-420f-8b5c-eb36ecd74298"
        },
        {
            "id": "52015b42-e536-4e86-aff9-4ad0a062078c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 60,
            "eventtype": 6,
            "m_owner": "a72be43a-bfbb-420f-8b5c-eb36ecd74298"
        },
        {
            "id": "a57dba9b-6372-48df-863e-df4e782dd5b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a72be43a-bfbb-420f-8b5c-eb36ecd74298"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
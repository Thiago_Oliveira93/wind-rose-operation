{
    "id": "2f4a35eb-b0e6-499b-aa64-a84f70bf6784",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_room_transition",
    "eventList": [
        {
            "id": "d41b240f-c2f3-495f-a2a9-3448ba0bb5d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f4a35eb-b0e6-499b-aa64-a84f70bf6784"
        },
        {
            "id": "83bd4cb9-66f9-4117-aa47-604b6d74adda",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "970e04d4-8106-4c4c-9479-f3a1922e0e12",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2f4a35eb-b0e6-499b-aa64-a84f70bf6784"
        }
    ],
    "maskSpriteId": "8f1ee375-f3e7-4ba5-a4a3-176c79921811",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ba399a06-de8d-4811-afee-c9f6374a898f",
    "visible": true
}
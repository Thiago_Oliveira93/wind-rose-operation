{
    "id": "f7e2edc2-109f-43e4-ac40-03542b4dff8e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_machinegun",
    "eventList": [
        {
            "id": "3279d2b3-fc27-43cf-b6b9-56689a938195",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f7e2edc2-109f-43e4-ac40-03542b4dff8e"
        },
        {
            "id": "2d2d1633-6422-48a8-9f75-e2498d4ea16b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f7e2edc2-109f-43e4-ac40-03542b4dff8e"
        },
        {
            "id": "1295f98b-04f6-47c4-b820-aea00ae02fb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f7e2edc2-109f-43e4-ac40-03542b4dff8e"
        },
        {
            "id": "07743d27-5369-4cd3-b155-31bf393ffa3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f7e2edc2-109f-43e4-ac40-03542b4dff8e"
        },
        {
            "id": "c14ae493-b501-4a24-aea5-a2fa508b9211",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "f7e2edc2-109f-43e4-ac40-03542b4dff8e"
        },
        {
            "id": "aadf333b-e8ce-42ec-974b-75169a27d5cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c95653b5-1fe1-4624-9b5b-b36e9f68b412",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f7e2edc2-109f-43e4-ac40-03542b4dff8e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d774852f-5f09-4dfd-b69b-f78f9c4410d3",
    "visible": true
}
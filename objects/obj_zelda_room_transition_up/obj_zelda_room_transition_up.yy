{
    "id": "ac685d1e-54ef-4dc8-8679-4cc8f1d69271",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_zelda_room_transition_up",
    "eventList": [
        {
            "id": "70f4cb76-ff7e-4063-ad49-fef83b978979",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ac685d1e-54ef-4dc8-8679-4cc8f1d69271"
        },
        {
            "id": "fa73e1a4-e1f0-49c5-ab63-eefdd56f5338",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ac685d1e-54ef-4dc8-8679-4cc8f1d69271"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b36916fd-0958-4ed8-bf22-ededdfe0cbdd",
    "visible": true
}
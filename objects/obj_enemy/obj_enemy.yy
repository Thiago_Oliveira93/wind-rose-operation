{
    "id": "2bcf7d05-f07f-4c95-a530-2360e9a701c8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "943a8726-2bec-4477-8bbd-26c87be6c52c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2bcf7d05-f07f-4c95-a530-2360e9a701c8"
        },
        {
            "id": "98088dc5-4817-46e4-b7db-1aebf6ec5d96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2bcf7d05-f07f-4c95-a530-2360e9a701c8"
        },
        {
            "id": "bd01401b-efc5-46ba-9df9-8f3f80078727",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2bcf7d05-f07f-4c95-a530-2360e9a701c8"
        },
        {
            "id": "ec905aaf-caf3-49e4-8f3a-b61ad12da98d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "2bcf7d05-f07f-4c95-a530-2360e9a701c8"
        },
        {
            "id": "0ff5e02f-d03c-4451-b34d-8bf3336ff8a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2bcf7d05-f07f-4c95-a530-2360e9a701c8"
        },
        {
            "id": "4069556e-ff53-4594-a512-4b53e9f8a090",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "2bcf7d05-f07f-4c95-a530-2360e9a701c8"
        },
        {
            "id": "feacd4e3-1b7b-4d8a-a539-e6342f350665",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8dd6467f-6779-44f4-b238-65f0b4240c1b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2bcf7d05-f07f-4c95-a530-2360e9a701c8"
        }
    ],
    "maskSpriteId": "c929a54e-0184-4ddb-9124-d04962cacca0",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3b6645d1-6af9-4616-8e39-eb910d61246f",
    "visible": true
}
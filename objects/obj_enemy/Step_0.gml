/// @description Enemy Behavior
scr_move_and_collide()

seeing_player = !collision_line(x+16,y+16,obj_player.x+16,obj_player.y+16,obj_wall,false,false)

if keyboard_check_pressed(vk_lshift)
{
	scr_change_affected()
}


my_vision.x = x+16
my_vision.y = y+16



if x > previous_x
{
	v_axis = 0
	h_axis = 1
	//	show_debug_message("direita")
	sprite_index = spr_soldier_walk_right
}
if x < previous_x
{
	v_axis = 0
	h_axis = -1
	//	show_debug_message("esquerda")
	sprite_index = spr_soldier_walk_left
}

if y > previous_y
{
	h_axis = 0
	v_axis = 1
	//show_debug_message("baixo")
	sprite_index = spr_soldier_walk_down
}

if y < previous_y
{
	h_axis = 0
	v_axis = -1
	//show_debug_message("cima")
	sprite_index = spr_soldier_walk_up
}



///Time effect (or not) 

if time_affected
{

	//if !obj_player.moving and !obj_player.Kadvance
	//{
	
	//	if !obj_player.Kadvance
	//	{
			if path_speed> 0
			{
				timer_path+=1
			}
			if path_speed<0
			{
				timer_path-=1
			}
			path_speed = 0.000000001
		//}
		if obj_player.moving or obj_player.Kadvance
		{
				//obj_player.Kadvance-=0.1
				path_speed = sign(timer_path)*patrol_time_affected_speed
				timer_path = 0
		
		}
	
	//}
	
}

if !time_affected
{
	
		if path_speed == 0 //or obj_player.advance
		{
			path_speed = sign(timer_path)*patrol_speed
			timer_path = 0

		}
	
}


if stationary == true
{
	scr_look_patrol()
}




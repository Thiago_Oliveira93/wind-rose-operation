{
    "id": "4984cc63-4d36-47f5-b773-0a427773e498",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_activated",
    "eventList": [
        {
            "id": "5098fcae-91ea-479f-a944-1e259950712b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4984cc63-4d36-47f5-b773-0a427773e498"
        },
        {
            "id": "a8c18476-9325-4aa7-b831-5f21ef84f1d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4984cc63-4d36-47f5-b773-0a427773e498"
        },
        {
            "id": "5dce2788-b098-4d4f-bd3c-26613c3205a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4984cc63-4d36-47f5-b773-0a427773e498"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "668d061c-336f-4a04-b67f-74d6a1ab0235",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "882fd9dd-ca0f-488a-8ecb-a7bb7cc65338",
    "visible": true
}
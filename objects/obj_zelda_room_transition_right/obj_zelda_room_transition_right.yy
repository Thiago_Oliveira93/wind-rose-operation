{
    "id": "1cb85e38-f980-44fd-a6b7-6935422a30a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_zelda_room_transition_right",
    "eventList": [
        {
            "id": "4b276569-67f9-4ed8-bdb5-3337a8217074",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1cb85e38-f980-44fd-a6b7-6935422a30a6"
        },
        {
            "id": "422eee6c-5fc4-413a-8829-88cd7aa2fc01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1cb85e38-f980-44fd-a6b7-6935422a30a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b36916fd-0958-4ed8-bf22-ededdfe0cbdd",
    "visible": true
}
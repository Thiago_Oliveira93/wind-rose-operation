/// @description Insert description here
///
if !obj_player.block_enable exit;

x = round(mouse_x/32)*32
y = round(mouse_y/32)*32

if instance_exists(obj_player)
{
	depth = obj_player.depth+1
	
	///deactivate for some time
	if obj_player.moving
	{
		visible = false
	}
	else
	{
		visible = true
	}
}


//selecting the block
if mouse_wheel_down() //or mouse_wheel_up()
{
	switch (sprite_index)
	{
		case spr_direction_changer:
		sprite_index = spr_direction_changer_down
		break;
		
		case spr_direction_changer_down:
		sprite_index = spr_direction_changer_left
		break;
		
		case spr_direction_changer_left:
		sprite_index = spr_direction_changer_up
		break;
		
		case spr_direction_changer_up:
		sprite_index = spr_direction_stop
		break;
		
		case spr_direction_stop:
		sprite_index = spr_direction_changer
		break;
		
	}
	
}



///placing the block
if obj_player.moving == false and !obj_player.k_aim
{
if !place_meeting(x,y,obj_wall) and !place_meeting(x,y,obj_player)
{
	
	if obj_player.blocks_hits>0
	{
		
		if sprite_index == spr_direction_changer
		{
			if obj_player.shoot_
			{
				instance_create_depth(x,y,50,obj_direction_changer_right)
				obj_player.blocks_hits -=1
			}
		}
		if sprite_index == spr_direction_changer_left
		{
			if obj_player.shoot_
			{
			instance_create_depth(x,y,50,obj_direction_changer_left)
			obj_player.blocks_hits -=1
			}
		}
		if sprite_index == spr_direction_changer_down
		{
			if obj_player.shoot_
			{
			instance_create_depth(x,y,50,obj_direction_changer_down)
			obj_player.blocks_hits -=1
			}
		}
		if sprite_index == spr_direction_changer_up
		{
			if obj_player.shoot_
			{
			instance_create_depth(x,y,50,obj_direction_changer_up)
			obj_player.blocks_hits -=1
			}
		}
	
		if sprite_index == spr_direction_stop
		{
			if obj_player.shoot_
			{
			instance_create_depth(x,y,50,obj_direction_stopper)
			obj_player.blocks_hits -=1
			}
		}
	}
	
	


}
}

//if obj_player.blocks_hits==0
//	{
//		visible = false
//	}
//	else
//	{
//		visible = true
//	}


{
    "id": "2881b780-712f-4846-9c48-99c8210e5975",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_game",
    "eventList": [
        {
            "id": "2b2c6f29-f33c-4efd-94f2-e6e18fc227b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2881b780-712f-4846-9c48-99c8210e5975"
        },
        {
            "id": "ca041026-f574-4df8-b933-da95e5bc390a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2881b780-712f-4846-9c48-99c8210e5975"
        },
        {
            "id": "bc0157b2-8158-468b-b6ec-46948c82c8ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2881b780-712f-4846-9c48-99c8210e5975"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c929a54e-0184-4ddb-9124-d04962cacca0",
    "visible": true
}
{
    "id": "e27bd563-207d-456a-bc10-d5c65fd3d2ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_door_button",
    "eventList": [
        {
            "id": "17a8f5f9-56ca-4fa3-acb6-bcf509f2292f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e27bd563-207d-456a-bc10-d5c65fd3d2ed"
        },
        {
            "id": "c86c4143-4308-4a8a-bdee-d9c81cbe2f2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e27bd563-207d-456a-bc10-d5c65fd3d2ed"
        },
        {
            "id": "49499109-a73e-43ab-be9b-f18c092e5be7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e27bd563-207d-456a-bc10-d5c65fd3d2ed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "01b8fc5a-ff3b-4971-a75e-32134982295d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cbadbad1-98f2-471b-b0c4-4beff93c08bc",
    "visible": true
}
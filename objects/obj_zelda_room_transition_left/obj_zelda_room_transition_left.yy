{
    "id": "bd7d7727-562e-40b2-acfe-ae21ca9779b4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_zelda_room_transition_left",
    "eventList": [
        {
            "id": "22c714f1-6493-455e-aaba-f6126d2ee11a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bd7d7727-562e-40b2-acfe-ae21ca9779b4"
        },
        {
            "id": "b1ec1291-1bff-48b7-ae7e-6b9e608253cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bd7d7727-562e-40b2-acfe-ae21ca9779b4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b36916fd-0958-4ed8-bf22-ededdfe0cbdd",
    "visible": true
}
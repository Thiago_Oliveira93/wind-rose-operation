{
    "id": "bb584ef2-e2dd-4644-b81d-9f7b68cb1806",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_bullet",
    "eventList": [
        {
            "id": "6301b5a4-8d63-4c6b-80c4-86826f5bc464",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bb584ef2-e2dd-4644-b81d-9f7b68cb1806"
        },
        {
            "id": "0479840b-206b-4970-bad7-11dc27431a6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bb584ef2-e2dd-4644-b81d-9f7b68cb1806"
        },
        {
            "id": "7f8b3027-7cfb-4716-ab32-e5660c8eb225",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "01b8fc5a-ff3b-4971-a75e-32134982295d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bb584ef2-e2dd-4644-b81d-9f7b68cb1806"
        },
        {
            "id": "ec7f2bec-1be3-42ca-ad40-3f2cc9d9a5c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bb584ef2-e2dd-4644-b81d-9f7b68cb1806"
        },
        {
            "id": "b9b7282b-3022-47ce-b789-856a3fa90d4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2bcf7d05-f07f-4c95-a530-2360e9a701c8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bb584ef2-e2dd-4644-b81d-9f7b68cb1806"
        },
        {
            "id": "76d63c59-6f8e-445d-af9a-32ddfce26ad9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f7e2edc2-109f-43e4-ac40-03542b4dff8e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bb584ef2-e2dd-4644-b81d-9f7b68cb1806"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "63bed941-43e8-4871-a34c-bb35b7a1fe83",
    "visible": true
}
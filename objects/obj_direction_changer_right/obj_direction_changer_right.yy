{
    "id": "8f0f21df-a779-42dc-af06-5fa81146628d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_direction_changer_right",
    "eventList": [
        {
            "id": "c363981e-877c-4862-8aa5-19466d751e02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f0f21df-a779-42dc-af06-5fa81146628d"
        },
        {
            "id": "f9824b53-4e97-4f62-9d0b-7b7eb40ed553",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8f0f21df-a779-42dc-af06-5fa81146628d"
        },
        {
            "id": "16caaac1-4f23-4bef-93fd-d8d476eaa09b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8f0f21df-a779-42dc-af06-5fa81146628d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "eb36f2f6-c1ef-4476-99b6-787c9852a905",
    "visible": true
}
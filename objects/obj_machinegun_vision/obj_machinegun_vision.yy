{
    "id": "0f182a20-37a0-47ed-8aa3-c48229f9a192",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_machinegun_vision",
    "eventList": [
        {
            "id": "95c132a7-614f-4859-97a5-f959ff46dd89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0f182a20-37a0-47ed-8aa3-c48229f9a192"
        },
        {
            "id": "00123453-038d-4059-a13f-dfbe5c718da0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0f182a20-37a0-47ed-8aa3-c48229f9a192"
        },
        {
            "id": "0a87cdbc-f7a5-4d53-8924-a171d315ea2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "970e04d4-8106-4c4c-9479-f3a1922e0e12",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0f182a20-37a0-47ed-8aa3-c48229f9a192"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7c806d4a-8cd8-452f-aa51-4f1786abb31c",
    "visible": true
}